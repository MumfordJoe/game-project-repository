#pragma once
// Include Libraries
#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include <SpriteBatch.h>
#include "Input.h"

// Class Headers
#include "sfxManager.h"

// Misc. Headers
#include "eGameMode.h"
#include "varTypes.h"

enum class EMenuState
{
	// Menu-State Machine States //
	Play,		// Play the game
	PlayHard,	// Play the game in hard mode
	Help,		// Information such as keyboard controls
	Exit		// Close / exit the game
};

//------------------Main Menu Class Declaration Start//
class MainMenu
{
public:
	MainMenu(MyD3D& d3d);																														// Constructor

	void initMenu(int& difficulty);																												// Initialises the menu
	bool renderMenu(DirectX::SpriteBatch& batch, EGameMode& _eGameMode, MouseAndKeys& mouseAndKeys, SFXManager& sfxManager, int& difficulty);	// Starts and runs the menu
	void destroy();																																// Deletes this instance of the menu
private:
	void setMenuState(EMenuState newState, int& difficulty);																					// Sets the state of the menu
	void setMenuState(int newState, int& difficulty);																							// Sets the state of the menu
	void _eMenuStateSync(bool isSyncingInt);

	MyD3D& mD3D;

	EMenuState _eMenuState;																														// Which option of the menu the player/user is on.
	int _eMenuState_int;
	Text gameName;
	Text menuOptionPlay;
	Text menuOptionPlayHard;
	Text menuOptionHelp;
	Text menuOptionExit;
	Text gameControls;
	Text creatorName;
	Sprite menuArrow;
	bool keyDown_Select = true;
	bool keyDown_Choose = true;
	bool isRenderingControls = false;
};
//------------------Main Menu Class Declaration End//