// Class Headers
#include "coin.h"

// Misc Headers
#include "utilities.h"
#include "varTypes.h"

//------------------Coin Manager Class Declaration Start//
class CoinManager
{
public:
	CoinManager(MyD3D& d3d);														// Constructor
	~CoinManager();																	// Destructor

	void init();
	void spawnCoin();																// Creates a coin and adds it to the coin_instances vector array
	void spawnCoin(float xPos, float yPos);											// Creates a coin at a position and adds it to the coin_instances vector array
	void spawnTimer(float elapsed, float environmentSpeed, Sprite platformSpr);		// Creates coins at calculated intervals
	void checkDeleteCoin(Coin* coin, int i);										// Checks a coin's current state and deletes it when necessary
	void clearCoins();																// Deletes all coins and empties coin_instances
	void destroy();																	// Deletes this instance of CoinManager

	std::vector < Coin* > coin_instances{};											// Holds and points to all of the coins that have been created through spawnCoin()
	float coinSpawnTimer = 0;														// The time until the next coin is spawned

private:
	MyD3D& mD3D;

	float maxCoinSpawnTimerLength = GC::COIN_SPAWNTIMER_MAX;						// The maximum values for how long it takes until the next coin is spawned
	float minCoinSpawnTimerLength = GC::COIN_SPAWNTIMER_MIN;						// The minimum values for how long it takes until the next coin is spawned
	float maxCoinXPosition = GC::COIN_XPOS_MAX;										// The maximum x position the coin manager can spawn a coin at relative to a platform
	float minCoinXPosition = GC::COIN_XPOS_MIN;										// The minimum x position the coin manager can spawn a coin at relative to a platform
	float maxCoinYPosition = GC::COIN_YPOS_MAX;										// The maximum y position the coin manager can spawn a coin at relative to a platform
	float minCoinYPosition = GC::COIN_YPOS_MIN;										// The minimum y position the coin manager can spawn a coin at relative to a platform
};
//------------------Coin Manager Class Declaration End//