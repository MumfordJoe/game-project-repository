// Accompanying Header
#include "coinManager.h"

// Include Libraries
#include <cmath>

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Coin Manager Class Functions Start//
//////// Public: ////////

CoinManager::CoinManager(MyD3D& d3d)
	: mD3D(d3d)
{
	init();
}

CoinManager::~CoinManager()
{
	clearCoins();
}

void CoinManager::init()
{
	coinSpawnTimer = maxCoinSpawnTimerLength;
	clearCoins();
}

void CoinManager::spawnCoin()
{
	coin_instances.push_back(new Coin(mD3D));
}

void CoinManager::spawnCoin(float xPos, float yPos)
{
	coin_instances.push_back(new Coin(xPos, yPos, mD3D));
}

// Platform Spawning Code
void CoinManager::spawnTimer(float elapsed, float environmentSpeed, Sprite platformSpr)
{

	if (coinSpawnTimer <= 0)
	{
		float xPos = RandomNumberGenerator::getRandomFloat(minCoinXPosition, maxCoinXPosition);
		float yPos = RandomNumberGenerator::getRandomFloat(minCoinYPosition, maxCoinYPosition);

		if (platformSpr.getPosition().x + xPos < GC::SCREEN_RES.x)
		{
			xPos += GC::SCREEN_RES.x / 2;
		}

 		spawnCoin(platformSpr.getPosition().x + xPos, platformSpr.getPosition().y - yPos);

		coinSpawnTimer = RandomNumberGenerator::getRandomFloat(minCoinSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed), maxCoinSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed));

		//platformSpawnTimer = 60; //test
	}
	else
	{
		coinSpawnTimer -= elapsed;

		if (coinSpawnTimer < 0)
			coinSpawnTimer = 0;
	}
}

void CoinManager::checkDeleteCoin(Coin* coin, int i)
{
	DirectX::SimpleMath::Vector2 pos = coin->spr.getPosition();
	float width = (coin->spr.getTextureRect().right - coin->spr.getTextureRect().left) * coin->spr.getScale().x;
	if ((pos.x + width) < GC::SCREEN_RES.x * -0.5) //-100
	{
		coin_instances.erase(coin_instances.begin() + i);
		coin->deleteInstance();
	}

	if (coin->deleteMe == true)
	{
		coin_instances.erase(coin_instances.begin() + i);
		coin->deleteInstance();
	}
}

void CoinManager::clearCoins()
{
	int coinNo = coin_instances.size();
	for (int i = 0; i < coinNo; i++)
	{
		coin_instances[i]->deleteInstance();
	}
	coin_instances.clear();
}

void CoinManager::destroy()
{
	delete this;
}
//------------------Coin Manager Class Functions End//