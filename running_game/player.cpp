// Accompanying Header
#include "player.h"

// Misc. Headers
#include "utilities.h"

//------------------Player Class Functions Start//
//////// Public: ////////

// Default Constructor
Player::Player(MyD3D& d3d)
	:mD3D(d3d), spr_run(mD3D), spr_jump(mD3D), spr_fall(mD3D), spr_attack(mD3D), spr_cling(mD3D), spr_active(mD3D), playerCoinText(mD3D), coinSprite(mD3D), coinSprite_active(mD3D)
{
	init();
}

void Player::init()
{
	FileLoader fileLoader;

	// Load Running Sprite
	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLAYER_RUN, mD3D, spr_run);
	RECTF texR0 = spr_run.getTextureRect();
	spr_run.setOrigin(float(texR0.right - texR0.left) / 2.f, float(texR0.bottom - texR0.top) / 2.f);
	spr_run.setScale(1.0f, 1.0f);
	spr_run.setRotation(0);

	// Load Jumping Sprite
	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLAYER_JUMP, mD3D, spr_jump);
	RECTF texR1 = spr_jump.getTextureRect();
	spr_jump.setOrigin(float(texR1.right - texR1.left) / 2.f, float(texR1.bottom - texR1.top) / 2.f);
	spr_jump.setScale(1.0f, 1.0f);
	spr_jump.setRotation(0);

	// Load Falling Sprite
	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLAYER_FALL, mD3D, spr_fall);
	RECTF texR2 = spr_fall.getTextureRect();
	spr_fall.setOrigin(float(texR2.right - texR2.left) / 2.f, float(texR2.bottom - texR2.top) / 2.f);
	spr_fall.setScale(1.0f, 1.0f);
	spr_fall.setRotation(0);

	// Load Attacking Sprite
	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLAYER_ATTACK, mD3D, spr_attack);
	RECTF texR3 = spr_attack.getTextureRect();
	spr_attack.setOrigin(float(texR3.right - texR3.left) / 2.f, float(texR3.bottom - texR3.top) / 2.f);
	spr_attack.setScale(1.0f, 1.0f);
	spr_attack.setRotation(0);

	// Load Clinging Sprite
	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLAYER_CLING, mD3D, spr_cling);
	RECTF texR4 = spr_cling.getTextureRect();
	spr_cling.setOrigin(float(texR4.right - texR4.left) / 2.f, float(texR4.bottom - texR4.top) / 2.f);
	spr_cling.setScale(1.0f, 1.0f);
	spr_cling.setRotation(0);

	changeActiveSpriteTo(0);										// Sets the starting sprite (spr_active) to spr_run (as the player is running at first)
	spr_active.setPosition(startingPosX, GC::SCREEN_RES.y * 0.6f);	// Sets the position of the object when created.

	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_36, mD3D, playerCoinText);
	playerCoinText.setFillColor(GC::UI_COLOR);
	playerCoinText.setOrigin(0, 0);
	std::stringstream ss;
	ss << "  x " + std::to_string(playerCoins);
	playerCoinText.setString(ss.str());
	playerCoinText.setPosition(GC::SCREEN_RES.x * 0.04f, GC::SCREEN_RES.y * 0.105f);


	fileLoader.assignTexToSpr(FILELOC::SPRITE_COIN, mD3D, coinSprite);
	RECTF texRC = coinSprite.getTextureRect();
	coinSprite.setOrigin(float(texRC.right - texRC.left) / 2.f, float(texRC.bottom - texRC.top) / 2.f);
	coinSprite.setScale(0.75, 0.75);
	originalXScale = coinSprite.getScale().x;
	coinSprite.setPosition(playerCoinText.getPosition().x + 5, playerCoinText.getPosition().y + 25);	// Sets the position of the object when created.
	coinSprite_active = coinSprite;

	// Key Value Re-Init
	isOnGround = false;															// Whether the player is touching the ground or not
	canJump = false;															// Whether the player can jump
	isColliding = false;														// Whether the player is colliding with another object or not
	isColliding_Horizontally = false;											// Whether the player is colliding horizontally with another object or not
	dead = false;																// Whether the player has lost the game or not
	gravity_current = GC::PLAYER_GRAVITY;										// How much the player is currently affected by gravity's pull
	playerCoins = 0;
	playerKills = 0;
	attackAnimationTimer = attackAnimationLength;
	coinSpin = 0;
}

// Jumping Code
void Player::jump(float elapsed, DirectX::SimpleMath::Vector2 screenSz, bool smallJump, SFXManager& sfxManager)
{
	if (smallJump == false)
	{
		if (canJump == true)
		{
			gravity_current = (-(1) * GC::PLAYER_JUMPSPEED);
			if (!isColliding_Horizontally)
			{
				sfxManager.playSFX(9);
			}
		}
		canJump = false;
		isOnGround = false;
	}
	else
	{
		if (canJump == true)
		{
			gravity_current = (-(1) * (GC::PLAYER_JUMPSPEED / 1.5));
			if (!isColliding_Horizontally)
			{
				sfxManager.playSFX(2);
			}
		}
		canJump = false;
		isOnGround = false;
	}

}

// Attacking Code
void Player::attack(SFXManager& sfxManager)	
{
	if (attackAnimationTimer == 0)
	{
		attackAnimationTimer = attackAnimationLength;
	}
}

// Gravity Code
void Player::gravity(float elapsed, DirectX::SimpleMath::Vector2 screenSz)
{ 
	DirectX::SimpleMath::Vector2 pos = spr_active.getPosition();

	if (isOnGround == false)
	{
		gravity_current += (GC::PLAYER_GRAVITY * GC::GRAVITY_ACCELERATION);
		if ((pos.y) < (screenSz.y * 1.125f))
		{
			pos.y += gravity_current * elapsed;
		}
		else
		{
			dead = true;
		}
	}
	else
	{
		gravity_current = 0;
	}

	if (!isColliding)
	{
		isOnGround = false;
	}

	spr_active.setPosition(pos);
}

// Player Collision Code
void Player::collide(float elapsed, Sprite spr)
{
	DirectX::SimpleMath::Vector2 pos = spr_active.getPosition();
	DirectX::SimpleMath::Vector2 collidingPos = spr.getPosition();
	isColliding = playerCollider.isColliding(spr_active, spr, false, false);

	if (isColliding)
	{
		gravity_current = 0;

		pos.y -= GC::PLAYER_GRAVITY * 4.25 * elapsed;

		if ((pos.y) > collidingPos.y)	// + ((spr_active.getTextureRect().bottom - spr_active.getTextureRect().top) / 2) - (GC::PLAYER_GRAVITY * 4.25 * elapsed)
		{
			pos.x -= (environmentSpeedCopy) * 1.25 * elapsed;
			isColliding_Horizontally = true;
			if (currentSprite != 4)
			{
				changeActiveSpriteTo(4);
			}
		}
		else
		{
			isColliding_Horizontally = false;
		}

		canJump = true;
		isOnGround = true;
	}

	spr_active.setPosition(pos);
}

void Player::recover(float elapsed, DirectX::SimpleMath::Vector2 screenSz)
{
	DirectX::SimpleMath::Vector2 pos = spr_active.getPosition();
	if (pos.x < startingPosX)
	{
		speed = speed_recovery;
	}
	else
	{
		speed = 0;
	}
	if (!isColliding_Horizontally)
	{
		pos.x += speed * elapsed;
	}
	if (pos.x < (screenSz.x * -0.125f))
	{
		dead = true;
	}
	spr_active.setPosition(pos);
}

void Player::animate(float elapsed)
{
	// Animate the roll of the Player Character

	if (currentSprite != 4)
	{
		if (isOnGround || speed > 0)
		{
			spr_active.rotate(rotationSpeed);		//clamp(rotationSpeed * (environmentSpeedCopy / GC::ENVIRONMENTOBJECT_SPEED), 0, 45));
		}
		else
		{
			spr_active.rotate(rotationSpeed / 2);	//clamp((rotationSpeed / 2) * (environmentSpeedCopy / GC::ENVIRONMENTOBJECT_SPEED), 0, 45));
		}
	}

	// Attacking Sprite Animation Timer

	attackAnimationTimer -= elapsed;
	if (attackAnimationTimer < 0)
	{
		attackAnimationTimer = 0;
	}

	// Sprite Swapping

	if (attackAnimationTimer != 0)
	{
		if (currentSprite != 3)
		{
			changeActiveSpriteTo(3);
		}
	}
	else
	{
		if (isColliding_Horizontally == true)
		{
			if (currentSprite != 4)
			{
				changeActiveSpriteTo(4);
			}
		}
		else
		{
			if (currentSprite != 0)
			{
				changeActiveSpriteTo(0);
			}
		}
		if (!isOnGround)
		{
			if (!canJump && gravity_current < 0 && isColliding_Horizontally == false)
			{
				if (currentSprite != 1)
				{
					changeActiveSpriteTo(1);
				}
			}
			if ((!canJump && gravity_current > 0 && isColliding_Horizontally == false) || (canJump && gravity_current > (GC::PLAYER_GRAVITY * 22.5) && isColliding_Horizontally == false))
			{
				if (currentSprite != 2)
				{
					changeActiveSpriteTo(2);
				}
			}
		}
	}

	// Update Coin UI

	std::stringstream ss;
	ss << "  x " + std::to_string(playerCoins);
	playerCoinText.setString(ss.str());

	coinSpin += elapsed;
	coinSprite_active.setScale(abs(sin(coinSpin * 5) * originalXScale), coinSprite_active.getScale().y);
}

//Sprite Swapping Code
void Player::changeActiveSpriteTo(int sprRefNo)	
{
	Sprite newSpr(mD3D);
	if (sprRefNo == 0)
	{
		newSpr = spr_run;
	}
	if (sprRefNo == 1)
	{
		newSpr = spr_jump;
	}
	if (sprRefNo == 2)
	{
		newSpr = spr_fall;
	}
	if (sprRefNo == 3)
	{
		newSpr = spr_attack;
	}
	if (sprRefNo == 4)
	{
		newSpr = spr_cling;
	}

	DirectX::SimpleMath::Vector2 pos = spr_active.getPosition();
	float rotation = spr_active.getRotation();
	spr_active = newSpr;
	spr_active.setOrigin(newSpr.getOrigin());
	spr_active.setScale(newSpr.getScale());
	if (sprRefNo == 4)
	{
		spr_active.setRotation(0);
	}
	else
	{
		spr_active.setRotation(rotation);
	}
	spr_active.setPosition(pos);

	currentSprite = sprRefNo;
}

void Player::updateEnvironmentSpeedCopy(float environmentSpeedIn)
{
	environmentSpeedCopy = environmentSpeedIn;
}

void Player::giveCoins(int amount)
{
	playerCoins += amount;
}
//------------------Player Class Functions End//