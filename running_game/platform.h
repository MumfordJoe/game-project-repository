#pragma once

// Include Libraries

// Misc. Headers
#include "gameConst.h"
#include "utilities.h"

//------------------Platform Class Declaration Start//
class Platform
{
public:
	Platform(MyD3D& d3d);																			// Constructor
	Platform(float xPos, float yPos, float minScale, float maxScale, bool randScale, MyD3D& d3d);	// Copy Constructor for spawning at a certain position

	void move(float elapsed);																		// Moves the platform to the left at a constant speed
	void updateSpeed(float speedIn);
	void collide(float elapsed, Sprite playerSpr);													// Handles horizontal collision between the platform and the player
	void deleteInstance();																			// Calls the instance of the class to delete itself.

	MyD3D& mD3D;
	Sprite spr;																						// Platform's Sprite
	bool isColliding = false;																		// Whether the platform is colliding with another object or not
private:

	float speed = GC::ENVIRONMENTOBJECT_SPEED;																// The speed of the platform
	float width_min = GC::PLATFORM_WIDTH_MIN;														// The minimum width of the platform
	float width_max = GC::PLATFORM_WIDTH_MAX;														// The maximum width of the platform
	Collider platformCollider;																		// The platform object's collider
	float minHeight = 1.25f;
};
//------------------Platform Class Declaration Start//