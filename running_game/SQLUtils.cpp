#include <assert.h>
#include <string>
#include <math.h>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <algorithm>

#include "SQLUtils.h"
#include "utilities.h"

void Metrics::SortAndUpdatePlayerData() {
	//see if the player is already in the top ten
	struct {
		bool operator()(const PlayerData& a) const
		{
			return a.name == name;
		}
		std::string name;
	} customEquals;
	customEquals.name = name;
	std::vector<PlayerData>::iterator it = std::find_if(playerData.begin(), playerData.end(), customEquals);
	if (it != playerData.end())
	{
		//player already exists but is the score higher?
		if ((*it).score < score)
		{
			(*it).score = score;
			(*it).sessions += 1;
		}
	}
	else if (playerData.size() < 10) // was < 10
	{
		//player not in top number of scores and there are not set top number of scores yet anyway
		playerData.push_back(PlayerData{ name,score,sessions });
	}
	else if (playerData.back().score < score)
	{
		//player not in and there are ten
		playerData.back().name = name;
		playerData.back().score = score;
		playerData.back().sessions = sessions;
	}
	struct {
		bool operator()(const PlayerData& a, const PlayerData& b) const
		{
			return a.score > b.score;
		}
	} customLess;
	sort(playerData.begin(), playerData.end(), customLess);
}

bool Metrics::DBLoad(const std::string& path) {
	playerData.clear();
	bool exists;
	db.Init(path, exists);
	if (exists)
	{
		db.ExecQuery("SELECT * FROM GAME_INFO");
		string version = db.GetStr(0, "VERSION");
		if (version != Metrics::VERSION)
		{
			db.ExecQuery("DROP TABLE IF EXISTS HIGHSCORES");
			db.ExecQuery("DROP TABLE IF EXISTS GAME_INFO");
			db.ExecQuery("DROP TABLE IF EXISTS PLAYER_NAMES");
			exists = false;
		}
	}

	if (!exists)
	{
		db.ExecQuery("CREATE TABLE PLAYER_NAMES(" \
			"ID		INTEGER PRIMARY KEY,"\
			"NAME			TEXT	NOT NULL)");

		db.ExecQuery("CREATE TABLE HIGHSCORES(" \
			"ID		INTEGER PRIMARY KEY,"\
			"NAMEID			INT,"\
			"SCORE			INT		NOT NULL,"\
			"SESSIONS		INT		NOT NULL,"\
			"FOREIGN KEY(NAMEID) REFERENCES PLAYER_NAMES(ID))");

		db.ExecQuery("CREATE TABLE GAME_INFO("\
			"ID		INTEGER PRIMARY KEY,"\
			"VERSION		CHAR(10)	NOT NULL)");

		stringstream ss;
		ss << "INSERT INTO GAME_INFO (VERSION) "\
			"VALUES (" << Metrics::VERSION << ")";
		db.ExecQuery(ss.str());
		return false;
	}

	//Clear out the Player Data & setup all the Players
	playerData.clear();
	if (!GC::IS_ENCRYPTING)
	{
		db.ExecQuery("SELECT * FROM PLAYER_NAMES");
		for (size_t i = 0; i < db.results.size(); ++i)
			playerData.push_back(PlayerData{ db.GetStr(i,"NAME"), 0, 1, db.GetInt(i,"ID") });

		for (size_t i = 0; i < playerData.size(); ++i)
		{
			//get Score & Sessions
			stringstream sql;
			sql << "SELECT * FROM HIGHSCORES WHERE NAMEID = " << playerData[i].playerID;
			db.ExecQuery(sql.str());
			assert(db.results.size() == 1);
			playerData[i].score = db.GetInt(0, "SCORE");
			playerData[i].sessions = db.GetInt(0, "SESSIONS");
			playerData[i].oldScore = true;
		}
	}
	else
	{
		db.ExecQuery("SELECT * FROM PLAYER_NAMES");
		for (size_t i = 0; i < db.results.size(); ++i)
		{
			std::string nameIn = db.GetStr(i, "NAME");
			moduloDecrypt(nameIn, GC::ENCRYPTION_KEY);

			//std::string idIn = db.GetStr(i, "ID");
			//encryptDecrypt(idIn, GC::ENCRYPTION_KEY);

			//playerData.push_back(PlayerData{ nameIn, 0, 1, std::stoi(idIn) });
			playerData.push_back(PlayerData{ nameIn, 0, 1, db.GetInt(i, "ID") });
		}

		for (size_t i = 0; i < playerData.size(); ++i)
		{
			//get Score & Sessions
			stringstream sql;
			sql << "SELECT * FROM HIGHSCORES WHERE NAMEID = " << playerData[i].playerID;
			db.ExecQuery(sql.str());
			assert(db.results.size() == 1);

			std::string scoreIn = db.GetStr(0, "SCORE");
			//encryptDecrypt(scoreIn, GC::ENCRYPTION_KEY);
			playerData[i].score = std::stoi(scoreIn);

			std::string sessionsIn = db.GetStr(0, "SESSIONS");
			//encryptDecrypt(sessionsIn, GC::ENCRYPTION_KEY);
			playerData[i].sessions = std::stoi(sessionsIn);

			playerData[i].oldScore = true;
		}
	}
	return true;
}

bool Metrics::FileSave(const std::string& path) {

	if (!path.empty())
		filePath = path;
	ofstream fs;
	fs.open(filePath);
	if (fs.is_open() && fs.good())
	{
		fs << VERSION;
		for (size_t i = 0; i < playerData.size(); ++i)
		{
			fs << ' ' << playerData[i].name << ' ' << playerData[i].score << ' ' << playerData[i].sessions;
		}
		assert(!fs.fail());
		fs.close();
	}
	else
	{
		assert(false);
		return false;
	}
	return true;
}

bool Metrics::DBSave(const std::string& path) {
	db.ExecQuery("DELETE FROM PLAYER_NAMES");
	db.ExecQuery("DELETE FROM HIGHSCORES");
	
	stringstream ss;
	if (!GC::IS_ENCRYPTING)
	{
		for (size_t i = 0; i < playerData.size(); ++i)
		{
			ss.str("");
			ss << "INSERT INTO PLAYER_NAMES (NAME)" \
				<< "VALUES ('" << playerData[i].name << "')";
			db.ExecQuery(ss.str());
		}

		int pushVal = 0;
		for (size_t i = 0; i < playerData.size(); ++i)
		{
			if (playerData[i].oldScore)
			{
				ss.str("");
				ss << "INSERT INTO HIGHSCORES (NAMEID,SCORE,SESSIONS)" \
					<< "VALUES (" << playerData[i].playerID + pushVal << "," << playerData[i].score << "," << playerData[i].sessions << ")";
				db.ExecQuery(ss.str());
			}
			else
			{
				if (playerData[i].playerID == 0)
				{
					ss.str("");
					ss << "INSERT INTO HIGHSCORES (NAMEID,SCORE,SESSIONS)" \
						<< "VALUES (" << playerData[i].playerID + (playerData.size()) << "," << playerData[i].score << "," << playerData[i].sessions << ")";
					db.ExecQuery(ss.str());
					pushVal++;
					playerData[i].oldScore = true;
				}
				else
				{
					ss.str("");
					ss << "INSERT INTO HIGHSCORES (NAMEID,SCORE,SESSIONS)" \
						<< "VALUES (" << playerData[i].playerID + 1 << "," << playerData[i].score << "," << playerData[i].sessions << ")";
					db.ExecQuery(ss.str());
					pushVal++;
					playerData[i].oldScore = true;
				}

			}
		}
	}
	else
	{
		for (size_t i = 0; i < playerData.size(); ++i)
		{
			std::string nameIn = playerData[i].name;

			//if (playerData[i].oldScore)
			//{
				moduloEncrypt(nameIn, GC::ENCRYPTION_KEY);
			//}

			ss.str("");
			ss << "INSERT INTO PLAYER_NAMES (NAME)" \
				<< "VALUES ('" << nameIn << "')";
			db.ExecQuery(ss.str());
		}

		int pushVal = 0;
		for (size_t i = 0; i < playerData.size(); ++i)
		{
			//std::string scoreIn = std::to_string(playerData[i].score);
			//encryptDecrypt(scoreIn, GC::ENCRYPTION_KEY);
			//std::string sessionIn = std::to_string(playerData[i].sessions);
			//encryptDecrypt(sessionIn, GC::ENCRYPTION_KEY);

			if (playerData[i].oldScore)
			{
				ss.str("");
				ss << "INSERT INTO HIGHSCORES (NAMEID,SCORE,SESSIONS)" \
					<< "VALUES (" << playerData[i].playerID + pushVal << "," << playerData[i].score << "," << playerData[i].sessions << ")";
				db.ExecQuery(ss.str());
			}
			else
			{
				if (playerData[i].playerID == 0)
				{
					ss.str("");
					ss << "INSERT INTO HIGHSCORES (NAMEID,SCORE,SESSIONS)" \
						<< "VALUES (" << playerData[i].playerID + (playerData.size()) << "," << playerData[i].score << "," << playerData[i].sessions << ")";
					db.ExecQuery(ss.str());
					pushVal++;
					playerData[i].oldScore = true;
				}
				else
				{
					ss.str("");
					ss << "INSERT INTO HIGHSCORES (NAMEID,SCORE,SESSIONS)" \
						<< "VALUES (" << playerData[i].playerID + 1 << "," << playerData[i].score << "," << playerData[i].sessions << ")";
					db.ExecQuery(ss.str());
					pushVal++;
					playerData[i].oldScore = true;
				}

			}
		}
	}

	ss.str("");
	ss << "UPDATE GAME_INFO SET VERSION = " << Metrics::VERSION;
	db.ExecQuery(ss.str());
	db.SaveToDisk();
	return false;
}

bool Metrics::FileLoad(const std::string& path) {

	assert(!path.empty());
	filePath = path;
	ifstream fs;
	fs.open(filePath, ios::binary);
	if (fs.is_open() && fs.good())
	{
		string version;
		fs >> version;
		if (version == VERSION)
		{
			playerData.clear();
			while (!fs.eof()) {
				PlayerData d;
				fs >> d.name;
				assert(!d.name.empty());
				fs >> d.score;
				assert(d.score >= 0);
				playerData.push_back(d);
				fs >> d.sessions;
				assert(d.sessions >= 0);
				playerData.push_back(d);
			}
		}
		assert(!fs.fail());
		fs.close();
	}
	return false;
}

bool Metrics::DBClose()
{
	db.Close();
	return true;
}

bool Metrics::IsScoreInTopTen() {
	if (playerData.size() < 10)
		return true;
	return playerData.back().score < score;
}

void Metrics::Restart() {
	score = 0;
}

// Rounding Function Definition
double round_up(double value, int decimal_places)
{
	const double multiplier = std::pow(10.0, decimal_places);
	return std::ceil(value * multiplier) / multiplier;
}

// String Pair Function Definition
string stringPair(string first, string second)
{
	string pair;
	pair = first + second;
	return pair;
}

// String Truncation Function Definition
string truncate(std::string str, size_t width)
{
	if (str.length() > width)
		return str.substr(0, width);
	return str;
}

// Callback Function Definition
static int callback(void* pNotUsed, int numF, char** arrVals, char** arrNames)
{
	stringstream ss;
	int i;
	for (i = 0; i < numF; i++) {
		ss << arrNames[i] << " = " << (arrVals[i] ? arrVals[i] : "NULL") << "\n";
	}
	PrintToOutput(ss.str().c_str(), "");
	return 0;
}

// Callback Format Function Definition
float totalSpent = 0;
static int callbackFormat(void* pNotUsed, int numF, char** arrVals, char** arrNames)
{
	stringstream ss;
	//stringstream ss2;
	int i;
	string poundSign = "";
	string dash = "";
	for (i = 0; i < numF; i++) {
		if ((i % 2) == (1 % 2))
		{
			dash = " - ";
			poundSign = "";
		}
		else
		{
			dash = "";
			poundSign = "�";
			totalSpent = stof(arrVals[i]) + totalSpent;
		}
		ss << dash << poundSign << (arrVals[i] ? arrVals[i] : "NULL");
	}
	PrintToOutput(ss.str().c_str(), "");
	PrintToOutput("");
	return 0;
}

// Callback Fields Function Definition
static int callbackFields(void* pNotUsed, int numF, char** arrVals, char** arrNames)
{
	stringstream ss;
	int i;
	for (i = 0; i < numF; i++) {
		ss << arrNames[i] << "\n";
	}

	PrintToOutput(ss.str().c_str(), "");
	return 0;
}

// Callback Contents Function Definition
static int callbackContents(void* pNotUsed, int numF, char** arrVals, char** arrNames)
{
	stringstream ss;
	int i;
	for (i = 0; i < numF; i++) {
		ss << arrVals[i];
	}

	PrintToOutput(ss.str().c_str(), "");
	return 0;
}

// Run SQL Function Definition
void RunSQL(const string& sql,
	sqlite3* db,
	int (*pCallback)(void*, int, char**, char**) = nullptr,
	const string& okPrompt = "RunSQL",
	const string& errorPrompt = "RunSQL")
{
	assert(!sql.empty() && db);
	char* zErrMsg = nullptr;
	int rc = sqlite3_exec(db, sql.c_str(), pCallback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		PrintToOutput("SQL error: " + errorPrompt, zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		if (okPrompt == "All tables in database 'playerData' displayed.")
			PrintToOutput("");
		PrintToOutput("OK: " + okPrompt);
	}
}