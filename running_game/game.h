#pragma once

// Include Libraries
#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include <SpriteBatch.h>
//#include "SFML/Graphics.hpp"

// Class Headers
#include "coinManager.h"
#include "enemyManager.h"
#include "platformManager.h"
#include "projectileManager.h"
#include "obstacleManager.h"
#include "sfxManager.h"
#include "player.h"
#include "timer.h"

// Misc. Headers
#include "eGameMode.h"
#include "varTypes.h"

//------------------Game Class Declaration Start//
enum class EGameState
{
	Playing,
	Paused,
	GameEnd,
};

class Game
{
public:
	Game(MyD3D& d3d);																										// Constructor

	void initGame();																										// Initialises the game
	void initGame(int difficultyIn);																						// Initialises the game (with a custom difficulty)
	bool updateGame(EGameMode& _eGameMode, MouseAndKeys& mouseAndKeys, SFXManager& sfxManager, float dTime, int& scoreRef);	// Starts and runs the game
	void renderGame(DirectX::SpriteBatch& batch);
	void destroy();																											// Deletes this instance of the game
	void updateDifficulty();																								// Checks the status of the game and determines whether it's time to increase the difficulty
	void setDifficulty(int difficultyIn);																					// Changes the difficulty level (as well as updating the enivornment speed according to the level)
	int getDifficulty();																									// Returns the difficulty level
	void setGameState(EGameState newState);																					// Sets the state of the game
	EGameState getGameState();																								// Returns the state of the game (returns _eGameState)
private:
	void updateScore();

	EGameState _eGameState;																									// The variable that holds the state of the game, part of the state machine for game states
	MyD3D& mD3D;

	// Game Vars
	int difficultyLevel = 1;																								// The level of the difficulty of the game
	float environmentSpeed = GC::ENVIRONMENTOBJECT_SPEED;																	// The speed the environment move leftward (crates, platforms, coins.etc)
	Clock clock;																											// Game Clock Init
	Timer runTimer;																											// Timer Init
	Player player;																											// Player Character Init
	PlatformManager platformManager;																						// Platform Manager Init
	CoinManager coinManager;																								// Coin Manager Init
	EnemyManager enemyManager;																								// Enemy Manager Init
	ObstacleManager obstacleManager;																						// Obstacle Manager Init
	ProjectileManager projectileManager;																					// Projectile Manager Init
	int score = 0;																											// The current score of the current game
	Text scoreText;																											// The text that displays the integer above
};
//------------------Game Class Declaration End//