// Accompanying Header
#include "gameApp.h"

// Misc Headers
#include "gameConst.h"
// Class Headers
#include "intro.h"
#include "outro.h"

//------------------GameApp Class Functions Start//
//////// Public: ////////

// Constructor
GameApp::GameApp(MyD3D& d3d)
	:mD3D(d3d), game(mD3D), menu(mD3D), scoreboard(mD3D)
{
	setGameMode(EGameMode::Intro);
	pBatch = new DirectX::SpriteBatch(&d3d.GetDeviceCtx());
	//initGameApp();
}

void GameApp::initGameApp()
{

}

void GameApp::renderGameApp(MouseAndKeys& mouseAndKeys, float dTime)
{
	switch (_eGameMode)
	{
		case EGameMode::Intro:
		{
			// Init Menu
			if (!menuInit)
			{
				menu.initMenu(difficulty);
				musicManager.stopMusic();
				menuInit = true;
			}

			// Creating Menu Loop
			if (!menu.renderMenu(*pBatch, _eGameMode, mouseAndKeys, sfxManager, difficulty))
			{
				menuInit = false;
			}
			else
			{
				musicManager.playMusic(_eGameMode);
			}

			break;
		}
		case EGameMode::Game:
		{
			// Init Game
			if (!gameInit)
			{
				game.initGame(difficulty);
				musicManager.stopMusic();
				gameInit = true;
			}

			// Creating Game Loop
			if (game.updateGame(_eGameMode, mouseAndKeys, sfxManager, dTime, gameScore))
			{
				game.renderGame(*pBatch);
				musicManager.playMusic(_eGameMode);
			}
			else
			{
				gameInit = false;
			}

			break;
		}
		case EGameMode::Outro:
		{
			// Init Scoreboard
			if (!scoreboardInit)
			{
				scoreboard.initScoreboard(gameScore);
				musicManager.stopMusic();
				scoreboardInit = true;
			}

			// Creating Scoreboard Loop
			if (!scoreboard.renderScoreboard(*pBatch, _eGameMode, mouseAndKeys, sfxManager, dTime))
			{
				scoreboardInit = false;
			}
			else
			{
				musicManager.playMusic(_eGameMode);
			}

			break;
		}
	}
	musicManager.update();
	sfxManager.update();
}

void GameApp::exitGameApp()
{
	
}

EGameMode GameApp::getGameMode()
{
	return _eGameMode;
}

void GameApp::setGameMode(EGameMode newMode)
{
	_eGameMode = newMode;
}

//////// Private: ////////

//------------------GameApp Class Functions End//