// Accompanying Header
#include "intro.h"

// Misc. Headers
#include "utilities.h"
#include "gameConst.h"

//------------------Main Menu Class Functions Start//
//////// Public: ////////

// Constructor
MainMenu::MainMenu(MyD3D& d3d)
	:mD3D(d3d), gameName(mD3D), menuOptionPlay(mD3D), menuOptionPlayHard(mD3D), menuOptionHelp(mD3D), menuOptionExit(mD3D), menuArrow(mD3D), gameControls(mD3D), creatorName(mD3D), _eMenuState(EMenuState::Play), _eMenuState_int(0)
{
}

void MainMenu::initMenu(int& difficulty)
{
	FileLoader fileLoader;

	// Load Game Title
	fileLoader.assignFontToText(FILELOC::FONT_CHINYEN_68, mD3D, gameName);
	gameName.setFillColor(DirectX::SimpleMath::Vector4(0, 1, 0, 1)); // Green
	gameName.setString(GC::GAME_NAME);
	RECT texR4 = gameName.getGlobalBounds();
	gameName.setOrigin((texR4.right - texR4.left) / 2.f, (texR4.bottom - texR4.top / 2.f));
	gameName.setPosition(GC::SCREEN_RES.x * 0.4875, GC::SCREEN_RES.y * 0.225f);

	// Load Play Option (Normal)
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, menuOptionPlay);
	menuOptionPlay.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	menuOptionPlay.setString("Play [Normal Mode]");
	RECT texR3 = menuOptionPlay.getGlobalBounds();
	menuOptionPlay.setOrigin((texR3.right - texR3.left) / 2.f, (texR3.bottom - texR3.top / 2.f));
	menuOptionPlay.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.375f);

	// Load Play Option (Hard)
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, menuOptionPlayHard);
	menuOptionPlayHard.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	menuOptionPlayHard.setString("Play [Hard Mode]");
	RECT texR6 = menuOptionPlayHard.getGlobalBounds();
	menuOptionPlayHard.setOrigin((texR6.right - texR6.left) / 2.f, (texR6.bottom - texR6.top / 2.f));
	menuOptionPlayHard.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.525f);

	// Load Help Game Option
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, menuOptionHelp);
	menuOptionHelp.setFillColor(DirectX::SimpleMath::Vector4(1,1,1,1)); // White
	menuOptionHelp.setString("Help & Controls");
	RECT texR2 = menuOptionHelp.getGlobalBounds();
	menuOptionHelp.setOrigin((texR2.right - texR2.left) / 2.f, (texR2.bottom - texR2.top / 2.f));
	menuOptionHelp.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.675f);

	// Load Exit Game Option
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, menuOptionExit);
	menuOptionExit.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	menuOptionExit.setString("Exit");
	RECT texR1 = menuOptionExit.getGlobalBounds();
	menuOptionExit.setOrigin((texR1.right - texR1.left) / 2.f, (texR1.bottom - texR1.top / 2.f));
	menuOptionExit.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.825f);
	
	// Load Menu Arrow/Cursor
	fileLoader.assignTexToSpr(FILELOC::SPRITE_MENU_ARROW, mD3D, menuArrow);
	RECTF texR0 = menuArrow.getTextureRect();
	menuArrow.setOrigin(0, float(texR0.bottom - texR0.top) / 1.f);
	menuArrow.setScale(1.0f, 1.0f);
	menuArrow.setRotation(0);

	// Load Controls
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_36, mD3D, gameControls);
	gameControls.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	gameControls.setString("> W or UP Key - Large Jump \n> S or DOWN Key - Small Jump \n> Enter - Slime Spitting Attack \n> Hold any Jump Key to climb walls. \n\nRoll for as long as you can while avoiding crates and enemies!");
	RECT texR5 = gameControls.getGlobalBounds();
	gameControls.setOrigin((texR5.right - texR5.left) / 2.f, (texR5.bottom - texR5.top / 2.f));
	gameControls.setPosition(GC::SCREEN_RES.x * 0.492, GC::SCREEN_RES.y * 0.725f);

	// Load Creator Name
	fileLoader.assignFontToText(FILELOC::FONT_CHINYEN_28, mD3D, creatorName);
	creatorName.setFillColor(DirectX::SimpleMath::Vector4(0.15, 0.5, 0.15, 0.75)); // Translucent Dark Green
	creatorName.setString("by Joseph Mumford");
	RECT texRN = creatorName.getGlobalBounds();
	creatorName.setOrigin(0, (texRN.bottom - texRN.top / 2.f));
	creatorName.setPosition(GC::SCREEN_RES.x * 0.01, GC::SCREEN_RES.y * 0.975f);

	setMenuState(EMenuState::Play, difficulty);
	keyDown_Choose = true;
	keyDown_Select = true;
}

bool MainMenu::renderMenu(DirectX::SpriteBatch& batch, EGameMode& _eGameMode, MouseAndKeys& mouseAndKeys, SFXManager& sfxManager, int& difficulty)
{
	// Clear Window Screen
	mD3D.BeginRender(GC::MENU_COLOR);

	CommonStates dxstate(&mD3D.GetDevice());
	batch.Begin(DirectX::SpriteSortMode_Deferred, dxstate.NonPremultiplied());
	
	// Menu Navigation
	if (!isRenderingControls)
	{
		if (mouseAndKeys.IsPressed(VK_UP) || mouseAndKeys.IsPressed(VK_W))
		{
			if (keyDown_Select == false)
			{
				setMenuState(_eMenuState_int - 1, difficulty);
				sfxManager.playSFX(0);
			}
			keyDown_Select = true;
		}
		else
		{
			if (mouseAndKeys.IsPressed(VK_DOWN) || mouseAndKeys.IsPressed(VK_S))
			{
				if (keyDown_Select == false)
				{
					setMenuState(_eMenuState_int + 1, difficulty);
					sfxManager.playSFX(0);
				}
				keyDown_Select = true;
			}
			else
			{
				keyDown_Select = false;
			}
		}

		if ((mouseAndKeys.IsPressed(VK_RETURN)) || (mouseAndKeys.IsPressed(VK_SPACE)))
		{
			if (keyDown_Choose == false)
			{
				switch (_eMenuState)
				{
				case (EMenuState::Play):
					{
						_eGameMode = EGameMode::Game;
						batch.End();
						mD3D.EndRender();
						sfxManager.playSFX(1);
						return false;
						break;
					}
				case (EMenuState::PlayHard):
					{
						_eGameMode = EGameMode::Game;
						batch.End();
						mD3D.EndRender();
						sfxManager.playSFX(1);
						return false;
						break;
					}
				case (EMenuState::Help):
					{
						isRenderingControls = true;
						sfxManager.playSFX(1);
						break;
					}
				case (EMenuState::Exit):
					{
						_eGameMode = EGameMode::ExitGame;
						batch.End();
						mD3D.EndRender();
						return false;
						break;
					}
				}
			}
			keyDown_Choose = true;
		}
		else
		{
			keyDown_Choose = false;
		}
	}
	else
	{
		if ((mouseAndKeys.IsPressed(VK_RETURN)) || (mouseAndKeys.IsPressed(VK_SPACE)))
		{
			if (keyDown_Choose == false)
			{
				isRenderingControls = false;
				sfxManager.playSFX(4);
			}
			keyDown_Choose = true;
		}
		else
		{
			keyDown_Choose = false;
		}
	}

	// Drawing Menu Text
	gameName.draw(batch);
	creatorName.draw(batch);

	if (!isRenderingControls)
	{
		menuOptionPlay.draw(batch);
		menuOptionPlayHard.draw(batch);
		menuOptionHelp.draw(batch);
		menuOptionExit.draw(batch);
		menuArrow.draw(batch);
	}
	else
	{
		gameControls.draw(batch);
	}

	// Updating the Window
	batch.End();
	mD3D.EndRender();

	return true;
}

void MainMenu::destroy()
{
	delete this;
}

//////// Private: ////////

void MainMenu::setMenuState(EMenuState newState, int& difficulty)
{
	_eMenuState = newState;
	switch (_eMenuState)
	{
		case EMenuState::Play:
		{
			RECT menuOptionPlay_rect = menuOptionPlay.getGlobalBounds();
			menuArrow.setPosition(menuOptionPlay.getPosition().x + ((menuOptionPlay_rect.right - menuOptionPlay_rect.left) / 2) + 50, menuOptionPlay.getPosition().y + (menuOptionPlay_rect.bottom - menuOptionPlay_rect.top) / 2);
			difficulty = 1;
			break;
		}
		case EMenuState::PlayHard:
		{
			RECT menuOptionPlayHard_rect = menuOptionPlayHard.getGlobalBounds();
			menuArrow.setPosition(menuOptionPlayHard.getPosition().x + ((menuOptionPlayHard_rect.right - menuOptionPlayHard_rect.left) / 2) + 50, menuOptionPlayHard.getPosition().y + (menuOptionPlayHard_rect.bottom - menuOptionPlayHard_rect.top) / 2);
			difficulty = 2;
			break;
		}
		case EMenuState::Help:
		{
			RECT menuOptionHelp_rect = menuOptionHelp.getGlobalBounds();
			menuArrow.setPosition(menuOptionHelp.getPosition().x + ((menuOptionHelp_rect.right - menuOptionHelp_rect.left) / 2) + 50, menuOptionHelp.getPosition().y + (menuOptionHelp_rect.bottom - menuOptionHelp_rect.top) / 2);
			break;
		}
		case EMenuState::Exit:
		{
			RECT menuOptionExit_rect = menuOptionExit.getGlobalBounds();
			menuArrow.setPosition(menuOptionExit.getPosition().x + ((menuOptionExit_rect.right - menuOptionExit_rect.left) / 2) + 50, menuOptionExit.getPosition().y + (menuOptionExit_rect.bottom - menuOptionExit_rect.top) / 2);
			break;
		}
	}
	_eMenuStateSync(true);
}

void MainMenu::setMenuState(int newState, int& difficulty)
{
	_eMenuState_int = newState;

	if (newState < 0)
		_eMenuState_int = 3;
	if (newState > 3)
		_eMenuState_int = 0;

	switch (_eMenuState_int)
	{
		case 0:
		{
			RECT menuOptionPlay_rect = menuOptionPlay.getGlobalBounds();
			menuArrow.setPosition(menuOptionPlay.getPosition().x + ((menuOptionPlay_rect.right - menuOptionPlay_rect.left) / 2) + 50, menuOptionPlay.getPosition().y + (menuOptionPlay_rect.bottom - menuOptionPlay_rect.top) / 2);
			difficulty = 1;
			break;
		}
		case 1:
		{
			RECT menuOptionPlayHard_rect = menuOptionPlayHard.getGlobalBounds();
			menuArrow.setPosition(menuOptionPlayHard.getPosition().x + ((menuOptionPlayHard_rect.right - menuOptionPlayHard_rect.left) / 2) + 50, menuOptionPlayHard.getPosition().y + (menuOptionPlayHard_rect.bottom - menuOptionPlayHard_rect.top) / 2);
			difficulty = 2;
			break;
		}
		case 2:
		{
			RECT menuOptionHelp_rect = menuOptionHelp.getGlobalBounds();
			menuArrow.setPosition(menuOptionHelp.getPosition().x + ((menuOptionHelp_rect.right - menuOptionHelp_rect.left) / 2) + 50, menuOptionHelp.getPosition().y + (menuOptionHelp_rect.bottom - menuOptionHelp_rect.top) / 2);
			break;
		}
		case 3:
		{
			RECT menuOptionExit_rect = menuOptionExit.getGlobalBounds();
			menuArrow.setPosition(menuOptionExit.getPosition().x + ((menuOptionExit_rect.right - menuOptionExit_rect.left) / 2) + 50, menuOptionExit.getPosition().y + (menuOptionExit_rect.bottom - menuOptionExit_rect.top) / 2);
			break;
		}
	}
	_eMenuStateSync(false);
}

void MainMenu::_eMenuStateSync(bool isSyncingInt)
{
	if (isSyncingInt)
	{
		switch (_eMenuState)
		{
			case EMenuState::Play:
			{
				_eMenuState_int = 0;
				break;
			}
			case EMenuState::PlayHard:
			{
				_eMenuState_int = 1;
				break;
			}
			case EMenuState::Help:
			{
				_eMenuState_int = 2;
				break;
			}
			case EMenuState::Exit:
			{
				_eMenuState_int = 3;
				break;
			}
		}
	}
	else
	{
		switch (_eMenuState_int)
		{
			case 0:
			{
				_eMenuState = EMenuState::Play;
				break;
			}
			case 1:
			{
				_eMenuState = EMenuState::PlayHard;
				break;
			}
			case 2:
			{
				_eMenuState = EMenuState::Help;
				break;
			}
			case 3:
			{
				_eMenuState = EMenuState::Exit;
				break;
			}
		}
	}

}
//------------------Main Menu Class Functions End//