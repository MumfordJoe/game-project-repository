// Accompanying Header
#include "game.h"

// Include Headers
#include "CommonStates.h"
#include "Input.h"
using namespace DirectX;

// Class Headers
#include "platformManager.h"
#include "player.h"
#include "randomNumberGenerator.h"
#include "timer.h"

// Misc. Headers
#include "utilities.h"

//------------------Game Class Functions Start//
//////// Public: ////////

// Constructor
Game::Game(MyD3D& d3d)
	:mD3D(d3d), player(mD3D), runTimer(mD3D), platformManager(mD3D), coinManager(mD3D), enemyManager(mD3D), obstacleManager(mD3D), projectileManager(mD3D), scoreText(mD3D), _eGameState(EGameState::Playing)
{
	//mouseAndKeys.Initialise(WinUtil::Get().GetMainWnd(), true, false);
}

void Game::initGame()
{
	setGameState(EGameState::Playing);

	RandomNumberGenerator::seed();

	clock.restart();
	player.init();
	runTimer.init();
	platformManager.init();
	platformManager.spawnPlatform();																		// First Platform Init
	platformManager.spawnPlatform(GC::SCREEN_RES.x * 0.15f, GC::SCREEN_RES.y * 0.7f, 2.0f, 2.0f, false);	// Starting Platform Init
	obstacleManager.init();
	coinManager.init();
	enemyManager.init();
	projectileManager.init();
	score = 0;

	FileLoader fileLoader;

	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, scoreText);
	scoreText.setFillColor(GC::UI_COLOR);
	scoreText.setOrigin(0, 0);
	std::stringstream ss;
	ss << "SCORE: " + std::to_string(score);
	scoreText.setString(ss.str());
	scoreText.setPosition(GC::SCREEN_RES.x * 0.02f, GC::SCREEN_RES.y * 0.015f);

}

void Game::initGame(int difficultyIn)
{
	setGameState(EGameState::Playing);

	RandomNumberGenerator::seed();

	clock.restart();
	player.init();
	runTimer.init();
	platformManager.init();
	platformManager.spawnPlatform();																		// First Platform Init
	platformManager.spawnPlatform(GC::SCREEN_RES.x * 0.15f, GC::SCREEN_RES.y * 0.7f, 2.0f, 2.0f, false);	// Starting Platform Init
	obstacleManager.init();
	coinManager.init();
	enemyManager.init();
	projectileManager.init();
	score = 0;

	FileLoader fileLoader;

	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, scoreText);
	scoreText.setFillColor(GC::UI_COLOR);
	scoreText.setOrigin(0, 0);
	std::stringstream ss;
	ss << "SCORE: " + std::to_string(score);
	scoreText.setString(ss.str());
	scoreText.setPosition(GC::SCREEN_RES.x * 0.02f, GC::SCREEN_RES.y * 0.015f);

	difficultyIn = clamp(difficultyIn, 1, 10);
	setDifficulty(difficultyIn);
}

bool Game::updateGame(EGameMode& _eGameMode, MouseAndKeys& mouseAndKeys, SFXManager& sfxManager, float dTime, int& scoreRef)
{
		WinUtil& wu = WinUtil::Get();

		DirectX::SimpleMath::Vector2 screenSz = DirectX::SimpleMath::Vector2(wu.GetClientHeight(), wu.GetClientWidth());

		// Time-based Code
		float elapsed = clock.getElapsedTime();
		clock.restart();

		// Player Game Behaviour
		player.updateEnvironmentSpeedCopy(environmentSpeed);
		if (mouseAndKeys.IsPressed(VK_UP) || mouseAndKeys.IsPressed(VK_W))
		{
			player.jump(dTime, screenSz, false, sfxManager);
		}
		if (mouseAndKeys.IsPressed(VK_DOWN) || mouseAndKeys.IsPressed(VK_S) || mouseAndKeys.IsPressed(VK_SPACE))
		{
			player.jump(dTime, screenSz, true, sfxManager);
		}
		if (mouseAndKeys.IsPressed(VK_RETURN))
		{
			player.attack(sfxManager);
			projectileManager.spawnProjectile(player, sfxManager);
		}
		player.gravity(dTime, screenSz);
		player.recover(dTime, screenSz);
		player.animate(dTime);

		// Platform Game Behaviour
		Sprite lastSpawnedPlatform(mD3D);
		int platformI = 0;

		for (size_t i = 0; i < platformManager.platform_instances.size(); i++)
		{
			// Player Collision with Platform
			player.collide(dTime, platformManager.platform_instances[i]->spr);
			platformManager.platform_instances[i]->collide(dTime, player.spr_active);

			// Platform Movement
			platformManager.platform_instances[i]->updateSpeed(environmentSpeed);
			platformManager.platform_instances[i]->move(dTime);

			// Platform Deletion upon exiting the screen
			platformManager.checkDeletePlatform(platformManager.platform_instances[i], i);

			platformI = i;
		}
		lastSpawnedPlatform = platformManager.platform_instances[platformI]->spr;

		// Platform Spawning
		platformManager.spawnTimer(dTime, environmentSpeed);

		// Coin Game Behaviour
		for (size_t i = 0; i < coinManager.coin_instances.size(); i++)
		{
			// Coin Collision with Platform
			coinManager.coin_instances[i]->collide(dTime, player, sfxManager);

			// Coin Movement
			coinManager.coin_instances[i]->updateSpeed(environmentSpeed);
 			coinManager.coin_instances[i]->move(dTime);

			// Coin Deletion upon exiting the screen
			coinManager.checkDeleteCoin(coinManager.coin_instances[i], i);
		}

		// Coin Spawning
		coinManager.spawnTimer(dTime, environmentSpeed, lastSpawnedPlatform);

		// Crate Game Behaviour
		for (size_t i = 0; i < obstacleManager.crate_instances.size(); i++)
		{
			// Player Collision with Crate
			player.collide(dTime, obstacleManager.crate_instances[i]->spr);
			obstacleManager.crate_instances[i]->collide(dTime, player.spr_active, true);

			// Crate Collision with Platform
			for (size_t a = 0; a < platformManager.platform_instances.size(); a++)
			{
				obstacleManager.crate_instances[i]->collide(dTime, platformManager.platform_instances[a]->spr, false);
			}

			// Crate Movement
			obstacleManager.crate_instances[i]->updateSpeed(environmentSpeed);
			obstacleManager.crate_instances[i]->move(dTime);
			obstacleManager.crate_instances[i]->gravity(dTime, screenSz);

			// Crate Deletion upon exiting the screen
			obstacleManager.checkDeleteCrate(obstacleManager.crate_instances[i], i, sfxManager);
		}

		// Crate Spawning
		obstacleManager.spawnTimer(dTime, environmentSpeed, lastSpawnedPlatform);

		// Player Projectile Game Behaviour
		for (size_t i = 0; i < projectileManager.projectile_instances.size(); i++)
		{
			// Player Projectile Collision with Crates
			for (size_t a = 0; a < obstacleManager.crate_instances.size(); a++)
			{
				if (projectileManager.projectile_instances[i]->collide(dTime, obstacleManager.crate_instances[a]))
				{
					// Spawn a coin where destroyed crate was
					DirectX::SimpleMath::Vector2 collidingPos = obstacleManager.crate_instances[i]->spr.getPosition();
					DirectX::SimpleMath::Vector2 crateCenter = collidingPos + DirectX::SimpleMath::Vector2(obstacleManager.crate_instances[a]->spr.getTextureRect().right / 2, obstacleManager.crate_instances[a]->spr.getTextureRect().bottom / 2);
					coinManager.spawnCoin(crateCenter.x, crateCenter.y - (GC::SCREEN_RES.y * 0.05));
				}
			}

			// Player Projectile Collision with Platforms
			for (size_t b = 0; b < platformManager.platform_instances.size(); b++)
			{
				projectileManager.projectile_instances[i]->collide(dTime, platformManager.platform_instances[b]);
			}

			// Player Projectile Movement
			projectileManager.projectile_instances[i]->move(dTime);
			projectileManager.projectile_instances[i]->gravity(dTime, screenSz);

			// Player Projectile Deletion Checks
			projectileManager.checkDeleteProjectile(projectileManager.projectile_instances[i], i);
		}

		// Player Projectile Cooldown
		projectileManager.projectileTimer(dTime);

		// Enemy Game Behaviour
		for (size_t i = 0; i < enemyManager.enemy_instances.size(); i++)
		{
			// Player Projectile Collision with Enemies
			for (size_t a = 0; a < projectileManager.projectile_instances.size(); a++)
			{
				projectileManager.projectile_instances[a]->collide(dTime, enemyManager.enemy_instances[i], player.playerKills);
			}

			// Enemy Movement & Attacks
			enemyManager.enemy_instances[i]->move(dTime);
			enemyManager.enemy_instances[i]->attackTimer(dTime, environmentSpeed, obstacleManager, sfxManager);

			// Enemy Deletion Checks
			enemyManager.checkDeleteEnemies(enemyManager.enemy_instances[i], i, sfxManager);
		}

		// Boss Game Behaviour
		for (size_t i = 0; i < enemyManager.boss_instances.size(); i++)
		{
			// Player Projectile Collision with Bosses
			for (size_t a = 0; a < projectileManager.projectile_instances.size(); a++)
			{
				projectileManager.projectile_instances[a]->collide(dTime, enemyManager.boss_instances[i], player.playerKills);
			}

			// Boss Movement & Attacks
			enemyManager.boss_instances[i]->move(dTime);
			enemyManager.boss_instances[i]->attackTimer(dTime, environmentSpeed, obstacleManager, sfxManager);

			// Boss Deletion Checks
			enemyManager.checkDeleteBosses(enemyManager.boss_instances[i], i, sfxManager);
		}

		// Enemy & Boss Spawning
		enemyManager.spawnTimer(dTime, environmentSpeed, difficultyLevel);

		// Timer Behaviour
		runTimer.update();

		// Score Update Check
		updateScore();

		// Difficulty Update Check
		updateDifficulty();

		if (player.dead)
		{
			_eGameMode = EGameMode::Outro;
			scoreRef = score;
			return false;
		}
		return true;
}

void Game::renderGame(DirectX::SpriteBatch& batch)
{
	// Clear Window Screen
	mD3D.BeginRender(DirectX::SimpleMath::Vector4(0.53,0.81,0.92,1)); // Sky Blue

	CommonStates dxstate(&mD3D.GetDevice());
	batch.Begin(DirectX::SpriteSortMode_Deferred, dxstate.NonPremultiplied());

	player.spr_active.draw(batch);

	for (size_t i = 0; i < obstacleManager.crate_instances.size(); i++)
	{
		// Crate Drawing
		obstacleManager.crate_instances[i]->spr.draw(batch);
	}

	for (size_t i = 0; i < enemyManager.enemy_instances.size(); i++)
	{
		// Enemy Drawing
		enemyManager.enemy_instances[i]->spr.draw(batch);
	}

	for (size_t i = 0; i < enemyManager.boss_instances.size(); i++)
	{
		// Enemy Drawing
		enemyManager.boss_instances[i]->spr.draw(batch);
	}

	for (size_t i = 0; i < projectileManager.projectile_instances.size(); i++)
	{
		// Player Projectile Drawing
		projectileManager.projectile_instances[i]->spr.draw(batch);
	}

	for (size_t i = 0; i < platformManager.platform_instances.size(); i++)
	{
		// Platform Drawing
		platformManager.platform_instances[i]->spr.draw(batch);
	}

	for (size_t i = 0; i < coinManager.coin_instances.size(); i++)
	{
		// Coin Drawing
		coinManager.coin_instances[i]->spr.draw(batch);
	}

	runTimer.text.draw(batch);

	player.playerCoinText.draw(batch);
	player.coinSprite_active.draw(batch);

	scoreText.draw(batch);

	// Updating the Window
	batch.End();
	mD3D.EndRender();
}

void Game::destroy()
{
	delete this;
}

void Game::updateDifficulty()
{
	if (runTimer.getMinutes() > difficultyLevel)
	{
		difficultyLevel += 1;
		environmentSpeed += GC::DIFFICULTY_SPEEDINCREASE;
	}
}

void Game::setDifficulty(int difficultyIn)
{
	difficultyLevel = difficultyIn;
	environmentSpeed = GC::ENVIRONMENTOBJECT_SPEED + (difficultyIn * GC::DIFFICULTY_SPEEDINCREASE);
}

int Game::getDifficulty()
{
	return difficultyLevel;
}

void Game::setGameState(EGameState newState)
{
	_eGameState = newState;
}

EGameState Game::getGameState()
{
    return _eGameState;
}

void Game::updateScore()
{
	score = ((runTimer.getTime()) + (player.playerCoins * GC::COIN_WORTH) + (player.playerKills * GC::ENEMY_WORTH));
	std::stringstream ss;
	ss << "SCORE: " + std::to_string(score);
	scoreText.setString(ss.str());
}
//------------------Game Class Functions  End//