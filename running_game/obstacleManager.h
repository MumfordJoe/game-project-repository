#pragma once
// Class Headers
#include "crate.h"
#include "sfxManager.h"

// Misc Headers
#include "utilities.h"
#include "varTypes.h"

//------------------Obstacle Manager Class Declaration Start//
class ObstacleManager
{
public:
	ObstacleManager(MyD3D& d3d);																// Constructor
	~ObstacleManager();																			// Destructor

	void init();
	void spawnCrate();																			// Creates a crate and adds it to the crate_instances vector array
	void spawnCrate(float xPos, float yPos, float minScale, float maxScale, bool randScale);	// Creates a crate at a position and adds it to the crate_instances vector array
	void spawnTimer(float elapsed, float environmentSpeed, Sprite platformSpr);					// Creates crates at calculated intervals
	void checkDeleteCrate(Crate* crate, int i, SFXManager& sfxManager);							// Checks a crate's current state and deletes it when necessary
	void clearObstacles();																		// Deletes all crates and empties crate_instances
	void destroy();																				// Deletes this instance of ObstacleManager

	std::vector < Crate* > crate_instances{};													// Holds and points to all of the crates that have been created through spawnCrate()
	float crateSpawnTimer = 0;																	// The time until the next crate is spawned

private:
	MyD3D& mD3D;

	float maxCrateSpawnTimerLength = GC::CRATE_SPAWNTIMER_MAX;									// The maximum values for how long it takes until the next crate is spawned
	float minCrateSpawnTimerLength = GC::CRATE_SPAWNTIMER_MIN;									// The minimum values for how long it takes until the next crate is spawned
	float maxCrateXPosition = GC::CRATE_XPOS_MAX;												// The maximum x position the obstacle manager can spawn a crate at relative to a platform
	float minCrateXPosition = GC::CRATE_XPOS_MIN;												// The minimum x position the obstacle manager can spawn a crate at relative to a platform
	float maxCrateYPosition = GC::CRATE_YPOS_MAX;												// The maximum y position the obstacle manager can spawn a crate at relative to a platform
	float minCrateYPosition = GC::CRATE_YPOS_MIN;												// The minimum y position the obstacle manager can spawn a crate at relative to a platform
	float maxCrateWidth = GC::CRATE_SCALE_MAX;													// The maximum scale a crate can be
	float minCrateWidth = GC::CRATE_SCALE_MIN;													// The minimum scale a crate can be
};
//------------------Obstacle Manager Class Declaration End//