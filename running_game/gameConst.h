#pragma once

#include <string>
#include "D3D.h"
#include "SimpleMath.h"

//------------------Dimensional Structures Start
// Dimensions in 2D that are integers
struct Dim2Di
{
	int x, y;
};

// Dimensions in 2D that are floats
struct Dim2Df
{
	float x, y;
};
//------------------Dimensional Structures End

//------------------Game Constants Start
//	A box to put Games Constants in.
//	These are special numbers with important meanings. (Screen width, 
//	ascii code for the escape key, number of lives a player starts with,
//	the name of the title screen music track, etc.)
namespace GC
{
	// General Constants
	const std::string GAME_NAME = { "Slimeball On A Roll" };				// The name of the game, displayed in the title screen and as the window name.
	const float GAME_SPEED = { 1 };											// The starting speed of the gameplay.
	const Dim2Di SCREEN_RES{ 1200,800 };									// The program's window resolution.
	const char ESCAPE_KEY{ 27 };
	const int FRAMERATE{ 60 };												// The framerate of the program.
	const bool MOUSEINPUTENABLED{ false };

	// Game Constants
	const int ENVIRONMENTOBJECT_SPEED{ 425 };								// The speed of all objects that move leftward.
	const float DIFFICULTY_SPEEDINCREASE{ ENVIRONMENTOBJECT_SPEED / 3 };	// How much the environment's speed increases by each time the difficulty increases.

	// Player Constants
	const float PLAYER_GRAVITY{ 6.0f };										// How much gravity is affecting the player. Previously 0.98.
	const float GRAVITY_ACCELERATION{ 3.95f };								// Previously 1.95f
	const int PLAYER_JUMPSPEED{ 850 };										// Older speeds include 1275 and 275000.
	const int PLAYER_SPEED{ 60 };
	const float PLAYER_STARTINGPOSX{ GC::SCREEN_RES.x * 0.25f };			// The starting x position of the player.
	const float PLAYER_ATTACKANIMATIONLENGTH{ 0.5f };						// How long the player remains with their attacking sprite after attacking.
	const float PLAYER_ROLLSPEED{ 12.0f };									// How fast the player character rolls.
	const DirectX::SimpleMath::Vector4 UI_COLOR{ 0,0.65f,0,0.75f };			// The colour of the UI in the game.
	const DirectX::SimpleMath::Vector4 MENU_COLOR{ 0,0.125f,0,1 };			// The colour of the main menu and scoreboard backgrounds.

	// Platform Constants

	const float PLATFORM_SPAWNTIMER_MAX{ 2 };								// In seconds.
	const float PLATFORM_SPAWNTIMER_MIN{ 1 };								// In seconds.
	const float PLATFORM_WIDTH_MAX{ 1.75f };								// The maximum width a platform can be.
	const float PLATFORM_WIDTH_MIN{ 0.75f };								// The minimum width a platform can be.
	const float PLATFORM_XPOS_MAX{ GC::SCREEN_RES.x * 1.45f };				// The maximum x position a platform can be.
	const float PLATFORM_XPOS_MIN{ GC::SCREEN_RES.x * 1.35f };				// The minimum x position a platform can be.
	const float PLATFORM_YPOS_MAX{ GC::SCREEN_RES.y * 0.85f };				// The maximum y position a platform can be.
	const float PLATFORM_YPOS_MIN{ GC::SCREEN_RES.y * 0.40f };				// The minimum y position a platform can be.

	// Coin Constants
	const int COIN_WORTH{ 25 };												// How many points coins are worth.
	const float COIN_SPAWNTIMER_MAX{ 12 };									// In seconds.
	const float COIN_SPAWNTIMER_MIN{ 6 };									// In seconds.
	const float COIN_XPOS_MAX{ GC::SCREEN_RES.x * 0.10f };					// The maximum x position a coin can be from a platform.
	const float COIN_XPOS_MIN{ GC::SCREEN_RES.x * -0.10f };					// The minimum x position a coin can be from a platform.
	const float COIN_YPOS_MAX{ GC::SCREEN_RES.y * 0.25f };					// The maximum y position a coin can be from a platform.
	const float COIN_YPOS_MIN{ GC::SCREEN_RES.y * 0.20f };					// The minimum y position a coin can be from a platform.

	// Crate Constants
	const float CRATE_GRAVITY{ PLAYER_GRAVITY * 3.25f };					// How much gravity is affecting crates.
	const float CRATE_SPAWNTIMER_MAX{ 10 };									// In seconds.
	const float CRATE_SPAWNTIMER_MIN{ 5 };									// In seconds.
	const float CRATE_SCALE_MAX{ 1.001f };									// The maximum scale a crate can be. Was 2.25f.
	const float CRATE_SCALE_MIN{ 1.0f };									// The minimum scale a crate can be. Was 0.75f.
	const float CRATE_XPOS_MAX{ GC::SCREEN_RES.x * 0.30f };					// The maximum x position a crate can be from a platform.
	const float CRATE_XPOS_MIN{ GC::SCREEN_RES.x * 0.20f };					// The minimum x position a crate can be from a platform.
	const float CRATE_YPOS_MAX{ GC::SCREEN_RES.y * 0.275f };				// The maximum y position a crate can be from a platform. Previously * 0.35f.
	const float CRATE_YPOS_MIN{ GC::SCREEN_RES.y * 0.25f };					// The minimum y position a crate can be from a platform. Previously * 0.20f.

	// Enemy Constants
	const int ENEMY_WORTH{ 30 };											// How many points defeating enemies is worth.
	const int ENEMY_SPAWNATLEVEL{ 1 };										// At which difficulty level enemies begin spawning.
	const int ENEMY_SPEED{ 120 };											// How fast the enemies move, both moving into frame and hovering left and right.
	const int ENEMY_HEALTH{ 1 };											// How many hits the enemies can take.
	const float ENEMY_SPAWNTIMER_MAX{ 30 };									// In seconds.
	const float ENEMY_SPAWNTIMER_MIN{ 15 };									// In seconds.
	const float ENEMY_XPOS_MAX{ GC::SCREEN_RES.x * 1.45f };					// The maximum x position an enemy can be.
	const float ENEMY_XPOS_MIN{ GC::SCREEN_RES.x * 1.35f };					// The minimum x position an enemy can be.
	const float ENEMY_YPOS_MAX{ GC::SCREEN_RES.y * 0.20f };					// The maximum y position an enemy can be.
	const float ENEMY_YPOS_MIN{ GC::SCREEN_RES.y * 0.10f };					// The minimum y position an enemy can be.
	const float ENEMY_ATTACKTIMER_MAX{ 5 };									// The maximum time it'll take for enemies to drop a crate.
	const float ENEMY_ATTACKTIMER_MIN{ 2 };									// The minimum time it'll take for enemies to drop a crate.
	const float ENEMY_HOVERINGWIDTH{ GC::SCREEN_RES.x * 0.33f };			// How far left and right enemies will hover.
	const float ENEMY_HOVERINGSPEED{ 1.15f };								// The speed at which enemies will hover horizontally across the screen.
	const float ENEMY_HOVERINGACCELERATION{ 1.25f };						// How fast enemies take to reach top hovering speed.

	// Boss Constants
	const int BOSS_SPAWNATLEVEL{ 3 };										// At which difficulty level bosses begin spawning.
	const int BOSS_SPEED{ 70 };												// How fast the bosses move, both moving into frame and hovering left and right.
	const int BOSS_HEALTH{ 3 };												// How many hits the bosses can take.
	const float BOSS_SPAWNTIMER_MAX{ 90 };									// In seconds.
	const float BOSS_SPAWNTIMER_MIN{ 50 };									// In seconds.
	const float BOSS_XPOS_MAX{ GC::SCREEN_RES.x * 1.45f };					// The maximum x position a boss can be.
	const float BOSS_XPOS_MIN{ GC::SCREEN_RES.x * 1.35f };					// The minimum x position a boss can be.
	const float BOSS_YPOS_MAX{ GC::SCREEN_RES.y * 0.25f };					// The maximum y position a boss can be.
	const float BOSS_YPOS_MIN{ GC::SCREEN_RES.y * 0.20f };					// The minimum y position a boss can be.
	const float BOSS_ATTACKTIMER_MAX{ 3.5f };								// The maximum time it'll take for bosses to drop a crate.
	const float BOSS_ATTACKTIMER_MIN{ 1.5f };								// The minimum time it'll take for bosses to drop a crate.
	const float BOSS_HOVERINGWIDTH{ GC::SCREEN_RES.x * 0.33f };				// How far left and right enemies will hover.
	const float BOSS_HOVERINGSPEED{ 1.0f };									// The speed at which bosses will hover horizontally across the screen.

	// Projectile Constants
	const float PROJECTILE_SPEED{ 500 };									// The xspeed of the projectiles.
	const float PROJECTILE_GRAVITY{ 4.0f };									// The strength of gravity on projectiles.
	const float PROJECTILE_COOLDOWN{ 0.25f };								// How long until the player can fire a new projectile once they've fired one.

	// Scoreboard Constants
	const std::string SCOREBOARD_VERSION = { "1.2" };						// The current version of the scoreboard, changing this will clear the scoreboard.
	const int SCOREBOARD_NUMBEROFSCORESSHOWN = { 10 };						// The number of scores displays on the high scores screen.
	const bool IS_ENCRYPTING{ true };										// Whether the scoreboard is encrypted or not. (Update SCOREBOARD_VERSION when changing this value.)
	const char ENCRYPTION_KEY{ 'J' };										// Set this to, and use, the Main Key: 'J'.
}
//------------------Game Constants End

//------------------File Locations Start
//	A box to put the file locations for assets in.
//	These include sprites, sound effects and music. They can be easily
//	altered from this namespace without having to change mutiple instances
//	of them in main.cpp.
namespace FILELOC 
{
	// Sprite Locations
	const std::string SPRITE_PLAYER_RUN{ "data/sprites/player_run.dds" };
	const std::string SPRITE_PLAYER_JUMP{ "data/sprites/player_jump.dds" };
	const std::string SPRITE_PLAYER_FALL{ "data/sprites/player_fall.dds" };
	const std::string SPRITE_PLAYER_ATTACK{ "data/sprites/player_attack.dds" };
	const std::string SPRITE_PLAYER_CLING{ "data/sprites/player_cling.dds" };
	const std::string SPRITE_MENU_ARROW{ "data/sprites/menu_arrow.dds" };

	const std::string SPRITE_PLATFORM{ "data/sprites/platform.dds" };
	const std::string SPRITE_COIN{ "data/sprites/coin.dds" };
	const std::string SPRITE_CRATE{ "data/sprites/crate.dds" };
	const std::string SPRITE_PROJECTILE{ "data/sprites/player_projectile.dds" };
	const std::string SPRITE_ENEMY{ "data/sprites/enemy.dds" };
	const std::string SPRITE_BOSS{ "data/sprites/boss.dds" };

	// Font Locations
	const std::wstring FONT_DIGITAL7_16{ L"data/fonts/digital-7_16.spritefont" };
	const std::wstring FONT_DIGITAL7_36{ L"data/fonts/digital-7_36.spritefont" };
	const std::wstring FONT_DIGITAL7_48{ L"data/fonts/digital-7_48.spritefont" };
	const std::wstring FONT_DIGITAL7_58{ L"data/fonts/digital-7_58.spritefont" };
	const std::wstring FONT_DIGITAL7_75{ L"data/fonts/digital-7_75.spritefont" };
	const std::wstring FONT_CHINYEN_28{ L"data/fonts/chinyen_28.spritefont" };
	const std::wstring FONT_CHINYEN_58{ L"data/fonts/chinyen_58.spritefont" };
	const std::wstring FONT_CHINYEN_68{ L"data/fonts/chinyen_68.spritefont" };

	// Music Locations
	const std::string MUSIC_MENU{ "menuMusic" };
	const std::string MUSIC_GAME{ "gameMusic" };
	const std::string MUSIC_SCOREBOARD { "scoreboardMusic" };

	// SFX Locations
	const std::string SFX_CHOOSE{ "choose" };
	const std::string SFX_SELECT{ "select" };
	const std::string SFX_FIRE{ "fire" };
	const std::string SFX_JUMP{ "jump" };
	const std::string SFX_RETURN{ "return" };
	const std::string SFX_RETURNDEEP{ "returnDeep" };
	const std::string SFX_CRATEDROP{ "crateDrop" };
	const std::string SFX_COIN{ "coin" };
	const std::string SFX_EXPLOSION{ "explosion" };
	const std::string SFX_JUMPHIGH{ "jumpHigh" };
	const std::string SFX_CRATEBREAK{ "crateBreak" };
	const std::string SFX_BOUNCE{ "bounce" };

	// Scoreboard Database Location
	const std::string SCOREBOARD{ "data/scoreboard/playerData.db" };
}
//------------------File Locations End