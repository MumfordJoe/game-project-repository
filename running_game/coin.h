#pragma once

// Include Libraries
#include "player.h"
#include "sfxManager.h"

// Misc. Headers
#include "gameConst.h"
#include "utilities.h"

//------------------Coin Class Declaration Start//
class Coin
{
public:
	Coin(MyD3D& d3d);																							// Constructor
	Coin(float xPos, float yPos, MyD3D& d3d);	// Copy Constructor for spawning at a certain position

	void move(float elapsed);																					// Moves the coin to the left at a constant speed
	void updateSpeed(float speedIn);
	void collide(float elapsed, Player& player, SFXManager& sfxManager);										// Handles horizontal collision between the coin and the player
	void deleteInstance();																						// Calls the instance of the class to delete itself.

	MyD3D& mD3D;
	Sprite spr;																									// Coin's Sprite
	bool isColliding = false;																					// Whether the coin is colliding with another object or not
	bool deleteMe = false;																						// When true, this instance is deleted by the coin manager

private:
	float speed = GC::ENVIRONMENTOBJECT_SPEED * 0.85;															// The speed of the coin
	Collider coinCollider;																						// The coin object's collider
	float coinSpin = 0;																							// Increments and spins the coin via changing it's x scale.
	float originalXScale;																						// Used to store the coin's scale in the x direction when initalised.
};
//------------------Coin Class Declaration Start//