#pragma once
#include "Input.h"

class Typing
{
public:
	void getKeyboard(std::string& stringIn, MouseAndKeys& mouseAndKeys)
	{
		std::wstring mssg;
		mouseAndKeys.GetPressedKeyNames(mssg);

		if (mssg != L"")
		{
			if (keyDown == false)
			{
				std::string keyboardOut = calcKeyboard(mouseAndKeys);
				if (keyboardOut != "NULL")
				{
					stringIn = keyboardOut;
				}
			}
			keyDown = true;
		}
		else
		{
			if (mouseAndKeys.IsPressed(VK_BACK) && stringIn.size() > 0)
			{
				if (keyDown == false)
				{
					stringIn.pop_back();
				}
				keyDown = true;
			}
			else
			{
				keyDown = false;
			}
		}
	}
private:
	bool keyDown = true;
	std::vector <std::string> inputList{
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
	};
	std::vector <std::string> inputList_caps{
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	};
	std::vector <int> VKList{
	0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,
	};
	std::string calcKeyboard(MouseAndKeys& mouseAndKeys)
	{
		if (mouseAndKeys.IsPressed(VK_SHIFT))
		{
			for (int i = 0; i < inputList_caps.size(); i++)
			{
				if (mouseAndKeys.IsPressed(VKList[i]))
				{
					return inputList_caps[i];
				}
			}
		}
		else
		{
			for (int i = 0; i < inputList.size(); i++)
			{
				if (mouseAndKeys.IsPressed(VKList[i]))
				{
					return inputList[i];
				}
			}
		}
		return "NULL";
	}
};