#pragma once

// Include Libraries
#include "AudioMgrFMOD.h"
#include "FileUtils.h"

// Misc. Headers
#include "gameConst.h"

//------------------SFX Manager Class Declaration Start//
class SFXManager
{
public:
	SFXManager()							// Constructor
	{
		File::initialiseSystem();
		audio.Initialise();
	};

	void playSFX(int sfxNumber)
	{
			switch (sfxNumber)
			{
			case (0):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_SELECT, false, false, &sfxHdl, 0.3f);
				break;
			}
			case (1):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_CHOOSE, false, false, &sfxHdl, 0.15f);
				break;
			}
			case (2):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_JUMP, false, false, &sfxHdl, 0.2f);
				break;
			}
			case (3):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_FIRE, false, false, &sfxHdl, 0.2f);
				break;
			}
			case (4):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_RETURN, false, false, &sfxHdl, 0.15f);
				break;
			}
			case (5):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_RETURNDEEP, false, false, &sfxHdl, 0.15f);
				break;
			}
			case (6):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_CRATEDROP, false, false, &sfxHdl, 0.2f);
				break;
			}
			case (7):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_COIN, false, false, &sfxHdl, 0.4f);
				break;
			}
			case (8):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_EXPLOSION, false, false, &sfxHdl, 0.3f);
				break;
			}
			case (9):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_JUMPHIGH, false, false, &sfxHdl, 0.2f);
				break;
			}
			case (10):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_CRATEBREAK, false, false, &sfxHdl, 0.2f);
				break;
			}
			case (11):
			{
				audio.GetSfxMgr()->Play(FILELOC::SFX_BOUNCE, false, false, &sfxHdl, 0.2f);
				break;
			}
			}
	}

	void update()
	{
		audio.Update();
	}

	//MyD3D& mD3D;

private:
	AudioMgrFMOD audio;
	unsigned int sfxHdl;
};
//------------------SFX Manager Class Declaration Start//