#pragma once

//------------------Random Number Generator Class Declaration Start//
class RandomNumberGenerator
{
public:
	static void seed();
	static float getRandomFloat(float min, float max) ;
};
//------------------Random Number Generator Class Declaration End//