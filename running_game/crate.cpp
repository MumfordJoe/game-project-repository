// Accompanying Header
#include "crate.h"

// Class Headers

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Crate Class Functions Start//
//////// Public: ////////

// Default Constructor
Crate::Crate(MyD3D& d3d)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_CRATE, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin(0, 0);
	float scale = RandomNumberGenerator::getRandomFloat(GC::CRATE_SCALE_MIN, GC::CRATE_SCALE_MAX);
	spr.setScale(scale, scale);
	spr.setPosition(GC::SCREEN_RES.x * 1.15f, GC::SCREEN_RES.y * 0.465f);	// Sets the position of the object when created.
}

Crate::Crate(float xPos, float yPos, float minScale, float maxScale, bool randScale, MyD3D& d3d)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_CRATE, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin(0, 0);
	float scale;
	if (randScale == true)
	{
		scale = RandomNumberGenerator::getRandomFloat(GC::CRATE_SCALE_MIN, GC::CRATE_SCALE_MAX);
	}
	else
	{
		scale = GC::CRATE_SCALE_MIN;
	}
	spr.setScale(scale, scale);
	spr.setPosition(xPos, yPos);	// Sets the position of the object when created.
}

// Movement Code
void Crate::move(float elapsed)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();
	pos.x -= speed * elapsed;
	spr.setPosition(pos);

	if (pos.y < 10)
	{
		deleteMe == true;
	}
}

// Gravity Code
void Crate::gravity(float elapsed, DirectX::SimpleMath::Vector2 screenSz)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();

	if (isOnGround == false)
	{
		gravity_current += (GC::CRATE_GRAVITY * GC::GRAVITY_ACCELERATION);
		if ((pos.y) < (screenSz.y * 1.125f))
		{
			pos.y += gravity_current * 2.25 * elapsed;
		}
	}
	else
	{
		gravity_current = 0;
	}

	if (!isColliding)
	{
		isOnGround = false;
	}

	spr.setPosition(pos);
}

// Collision Code
void Crate::collide(float elapsed, Sprite collidingSpr, bool isPlayer)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();
	DirectX::SimpleMath::Vector2 collidingPos = collidingSpr.getPosition();
	
	if (isPlayer)
	{
		isColliding = crateCollider.isColliding(collidingSpr, spr, false, false);

		if (isColliding)
		{
			//gravity_current = 0;
			if (((collidingPos.y) > (pos.y)) && ((collidingPos.y) < (pos.y + (collidingSpr.getTextureRect().bottom - collidingSpr.getTextureRect().top) * spr.getScale().y)))
			{
				collidingPos.x -= speed * 1.25 * elapsed;
			}
		}

		collidingSpr.setPosition(collidingPos);
	}
	else
	{
		isColliding = crateCollider.isColliding(spr, collidingSpr, true, false);

		if (isColliding)
		{
			gravity_current = 0;
			pos.y -= GC::PLAYER_GRAVITY * 0.9 * elapsed;

			if (pos.y - ((collidingSpr.getTextureRect().bottom - collidingSpr.getTextureRect().top) * spr.getScale().y) > (collidingPos.y))
			{
				pos.y += (speed);// *1.25 * elapsed;
			}

			isOnGround = true;
		}

		spr.setPosition(pos);
	}
}

void Crate::deleteInstance()
{
	delete this;
}

void Crate::updateSpeed(float speedIn)
{
	speed = speedIn;
}

//------------------Crate Class Functions End//