
// Accompanying Header
#include "projectileManager.h"

// Include Libraries
#include <cmath>

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Projectile Manager Class Functions Start//
//////// Public: ////////

ProjectileManager::ProjectileManager(MyD3D& d3d)
	: mD3D(d3d)
{
	init();
}

ProjectileManager::~ProjectileManager()
{
	clearProjectiles();
}

void ProjectileManager::init()
{
	clearProjectiles();
	projectileCooldown = GC::PROJECTILE_COOLDOWN;
}

void ProjectileManager::spawnProjectile(Player player, SFXManager& sfxManager)
{
	if ((projectileCooldown <= 0) && (projectile_instances.size() < 2))
	{
		projectileCooldown = GC::PROJECTILE_COOLDOWN;
		projectile_instances.push_back(new Projectile(mD3D, player.spr_active));
		sfxManager.playSFX(3);
	}
}


void ProjectileManager::projectileTimer(float elapsed)
{
	projectileCooldown -= elapsed;

	if (projectileCooldown < 0)
		projectileCooldown = 0;
}

void ProjectileManager::checkDeleteProjectile(Projectile* projectile, int i)
{
	DirectX::SimpleMath::Vector2 pos = projectile->spr.getPosition();
	//float width = (projectile->spr.getTextureRect().right - projectile->spr.getTextureRect().left) * projectile->spr.getScale().x;
	if ((pos.x) > GC::SCREEN_RES.x * 1.5) //-100
	{
		projectile_instances.erase(projectile_instances.begin() + i);
		projectile->deleteInstance();
	}

	if (projectile->deleteMe == true)
	{
		projectile_instances.erase(projectile_instances.begin() + i);
		projectile->deleteInstance();
	}
}

void ProjectileManager::clearProjectiles()
{
	int obstacleNo = projectile_instances.size();
	for (int i = 0; i < obstacleNo; i++)
	{
		projectile_instances[i]->deleteInstance();
	}
	projectile_instances.clear();
}

void ProjectileManager::destroy()
{
	delete this;
}
//------------------Projectile Manager Class Functions End//