// Accompanying Header
#include "varTypes.h"

//------------------Text Class Definition Start//
//////// Public: ////////

// Text Constructor
Text::Text(MyD3D& d3d)
	:depth(0), rotation(0), scale(1), mD3D(d3d), textFont(nullptr)
{
}

// Draws the text to window.
void Text::draw(DirectX::SpriteBatch& batch)
{
	std::stringstream ss;
	ss << string;
	textFont->DrawString(&batch, ss.str().c_str(), DirectX::SimpleMath::Vector2(pos.x, pos.y), color, rotation, DirectX::SimpleMath::Vector2(origin.x, origin.y), scale);
}

// Getters //

// Returns the position of the text.
const DirectX::SimpleMath::Vector2 Text::getPosition() const
{
	return pos;
}

// Returns the rotation of the text.
const float Text::getRotation() const
{
	return rotation;
}

// Returns the scale of the text.
const float Text::getScale() const
{
	return scale;
}

// Returns the origin of the text.
const DirectX::SimpleMath::Vector2 Text::getOrigin() const
{
	return origin;
}

// Setters //

// Sets the string of the text.
void Text::setString(std::string stringIn)
{
	string = stringIn;
}

// Sets the font of the text.
void Text::setFont(SpriteFont* font)
{
	textFont = font;
}

// Sets the position of the text.
void Text::setPosition(const DirectX::SimpleMath::Vector2& posIn)
{
	pos = posIn;
}

// Sets the position of the text.
void Text::setPosition(const float& posInX, const float& posInY)
{
	pos.x = posInX;
	pos.y = posInY;
}

// Sets the rotation of the text.
void Text::setRotation(const float rotationIn)
{
	rotation = rotationIn;
}

// Sets the scale of the text.
void Text::setScale(const float& scaleIn)
{
	scale = scaleIn;
}

// Sets the origin of the text.
void Text::setOrigin(const DirectX::SimpleMath::Vector2& originIn)
{
	origin = originIn;
}

// Sets the origin of the text.
void Text::setOrigin(const float& originInX, const float& originInY)
{
	origin.x = originInX;
	origin.y = originInY;
}

// Sets the color of the text.
void Text::setFillColor(DirectX::SimpleMath::Vector4 colorIn)
{
	color = colorIn;
}
//------------------Text Class Definition End//

//------------------Sprite Class Definition Start//
//////// Public: ////////

Sprite::Sprite(MyD3D& d3d)
	:pos(0, 0), vel(0, 0), depth(0), mTexRect(), colour(1, 1, 1, 1), rotation(0), scale(1, 1), origin(0, 0), mTexture(nullptr), mD3D(d3d), mpTexData(nullptr)
{

}

Sprite& Sprite::operator=(const Sprite& rhs) {
	pos = rhs.pos;
	vel = rhs.vel;
	depth = rhs.depth;
	mTexRect = rhs.mTexRect;
	colour = rhs.colour;
	rotation = rhs.rotation;
	scale = rhs.scale;
	origin = rhs.origin;
	mTexture = rhs.mTexture;
	return *this;
}

void Sprite::draw(SpriteBatch& batch)
{
	batch.Draw(mTexture, pos, &(RECT)mTexRect, colour, rotation, origin, scale, DirectX::SpriteEffects::SpriteEffects_None, depth);
}

//void Sprite::Scroll(float x, float y) 
//{
//	mTexRect.left += x;
//	mTexRect.right += x;
//	mTexRect.top += y;
//	mTexRect.bottom += y;
//}


// Getters //
const DirectX::SimpleMath::Vector2 Sprite::getPosition() const
{
	return pos;
}
const float Sprite::getRotation() const
{
	return rotation;
}

const DirectX::SimpleMath::Vector2 Sprite::getScale() const
{
	return scale;
}

const DirectX::SimpleMath::Vector2 Sprite::getOrigin() const
{
	return origin;
}

const RECTF Sprite::getTextureRect() const
{
	return mTexRect;
}

const TexCache::Data& Sprite::getTexData() const
{
	assert(mpTexData);
	return *mpTexData;
}

ID3D11ShaderResourceView& Sprite::getTex()
{
	assert(mTexture);
	return *mTexture;
}

const DirectX::SimpleMath::Vector4 Sprite::getColor()
{
	return colour;
}

// Setters //
void Sprite::setPosition(const DirectX::SimpleMath::Vector2& posIn)
{
	pos = posIn;
}

void Sprite::setPosition(const float& posInX, const float& posInY)
{
	pos.x = posInX;
	pos.y = posInY;
}

void Sprite::setRotation(const float rotationIn)
{
	rotation = rotationIn;
}

void Sprite::setScale(const DirectX::SimpleMath::Vector2& scaleIn)
{
	scale = scaleIn;
}

void Sprite::setScale(const float& scaleInX, const float& scaleInY)
{
	scale.x = scaleInX;
	scale.y = scaleInY;
	//mTexRect.bottom *= 2;
	//mTexRect.right *= 2;
}

void Sprite::setOrigin(const DirectX::SimpleMath::Vector2& originIn)
{
	origin = originIn;
}

void Sprite::setOrigin(const float& originInX, const float& originInY)
{
	origin.x = originInX;
	origin.y = originInY;
}

//void Sprite::setTextureRect(const RECT rectIn)
//{
//	mTexRect = rectIn;
//}

void Sprite::setTextureRect(const float left, const float top, const float right, const float bottom)
{
	mTexRect.left = left;
	mTexRect.top = top;
	mTexRect.right = right;
	mTexRect.bottom = bottom;
}

void Sprite::setTextureRect(const RECTF& texRect)
{
	mTexRect.left = texRect.left;
	mTexRect.right = texRect.right;
	mTexRect.top = texRect.top;
	mTexRect.bottom = texRect.bottom;
}

void Sprite::setTexture(ID3D11ShaderResourceView& tex, const RECTF& texRect)
{
	mTexture = &tex;
	mpTexData = &mD3D.GetCache().Get(mTexture);

	if (mTexRect.left == mTexRect.right && mTexRect.top == mTexRect.bottom)
	{
		setTextureRect(RECTF{ 0,0,mpTexData->dim.x,mpTexData->dim.y });
	}
}

void Sprite::setTexture(ID3D11ShaderResourceView* tex, const RECTF& texRect)
{
	mTexture = tex;
	mpTexData = &mD3D.GetCache().Get(mTexture);

	if (mTexRect.left == mTexRect.right && mTexRect.top == mTexRect.bottom)
	{
		setTextureRect(RECTF{ 0,0,mpTexData->dim.x,mpTexData->dim.y });
	}
}

void Sprite::setColor(DirectX::SimpleMath::Vector4 colorIn)
{
	colour = colorIn;
}

void Sprite::setColor(float r, float g, float b, float a)
{
	colour.w = a;
	colour.x = r;
	colour.y = g;
	colour.z = b;
}
//------------------Sprite Class Definition End//