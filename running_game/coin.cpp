// Accompanying Header
#include "coin.h"

// Class Headers

//------------------Coin Class Functions Start//
//////// Public: ////////

// Default Constructor
Coin::Coin(MyD3D& d3d)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_COIN, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin(float(texR.right - texR.left) / 2.f, float(texR.bottom - texR.top) / 2.f);
	spr.setScale(1, 1);
	originalXScale = spr.getScale().x;
	spr.setPosition(GC::SCREEN_RES.x * 1.15f, GC::SCREEN_RES.y * 0.465f);	// Sets the position of the object when created.
}

Coin::Coin(float xPos, float yPos, MyD3D& d3d)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_COIN, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin(float(texR.right - texR.left) / 2.f, float(texR.bottom - texR.top) / 2.f);
	spr.setScale(1, 1);
	originalXScale = spr.getScale().x;
	spr.setPosition(xPos, yPos);	// Sets the position of the object when created.
}

void Coin::move(float elapsed)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();
	pos.x -= speed * elapsed;
	spr.setPosition(pos);

	if (pos.x < 10)
	{
		deleteMe = true;
	}

	coinSpin += elapsed;
	spr.setScale(abs(sin(coinSpin * 5) * originalXScale), spr.getScale().y);
}

void Coin::deleteInstance()
{
	delete this;
}

void Coin::updateSpeed(float speedIn)
{
	speed = speedIn * 0.85;
}

void Coin::collide(float elapsed, Player& player, SFXManager& sfxManager)
{
	isColliding = coinCollider.isColliding(player.spr_active, spr, false, true);
	if (isColliding)
	{
		player.giveCoins(1);
		sfxManager.playSFX(7);
		deleteMe = true;
	}
}

//////// Private: ////////
//------------------Coin Class Functions End//