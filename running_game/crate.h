#pragma once

// Include Libraries

// Misc. Headers
#include "gameConst.h"
#include "utilities.h"

//------------------Crate Class Declaration Start//
class Crate
{
public:
	Crate(MyD3D& d3d);																			// Constructor
	Crate(float xPos, float yPos, float minScale, float maxScale, bool randScale, MyD3D& d3d);	// Copy Constructor for spawning at a certain position

	void move(float elapsed);																	// Moves the crate to the left at a constant speed
	void gravity(float elasped, DirectX::SimpleMath::Vector2 screenSz);
	void updateSpeed(float speedIn);
	void collide(float elapsed, Sprite collidingSpr, bool isPlatform);							// Handles horizontal collision between the crate and the player
	void deleteInstance();																		// Calls the instance of the class to delete itself.

	MyD3D& mD3D;
	Sprite spr;																					// Crate's Sprite
	bool isOnGround = false;
	bool isColliding = false;																	// Whether the crate is colliding with another object or not
	bool deleteMe = false;
private:

	float gravity_current = GC::CRATE_GRAVITY;
	float speed = GC::ENVIRONMENTOBJECT_SPEED;;													// The speed of the crate
	Collider crateCollider;																		// The crate object's collider
};
//------------------Crate Class Declaration Start//