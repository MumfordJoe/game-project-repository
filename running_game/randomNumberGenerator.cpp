// Accompanying Header
#include "randomNumberGenerator.h"

// Include Libraries
#include <stdlib.h>
#include <assert.h>
#include <ctime>

//------------------Random Number Generator Class Functions Start//
//////// Public: ////////

void RandomNumberGenerator::seed()
{
	srand(static_cast<unsigned>(time(0)));
}

float RandomNumberGenerator::getRandomFloat(float min, float max) {
	assert(min < max);
	float alpha = rand() / (float)RAND_MAX;
	alpha = min + (max - min) * alpha;
	return alpha;
}
//------------------Random Number Generator Functions Class End//