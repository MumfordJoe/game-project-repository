// Accompanying Header
#include "timer.h"

// Include Libraries
//#include "SFML/Graphics.hpp"
#include <iomanip>
#include <sstream>

// Misc. Headers
#include "gameConst.h"
#include "utilities.h"

//------------------Timer Class Functions Start//
//////// Public: ////////

// Default Constructor
Timer::Timer(MyD3D& d3d)
	: hours(0), minutes(0), seconds(0), mD3D(d3d), text(mD3D)
{
	init();
}
// Copy Constructor
Timer::Timer(MyD3D& d3d, const Timer& t)
	: hours(t.hours), minutes(t.minutes), seconds(t.seconds), mD3D(d3d), text(mD3D)
{
}

// H.M.S constructor
Timer::Timer(MyD3D& d3d, const int h, const int m, const int s)
	: hours(h), minutes(m), seconds(s), mD3D(d3d), text(mD3D)
{
}

// Seconds Constructor
Timer::Timer(MyD3D& d3d, const long s)
	: hours(s / 3600), minutes((s / 60) % 60), seconds(s % 60), mD3D(d3d), text(mD3D)
{
}

// Destructor
Timer::~Timer()
{
}

void Timer::init()
{
	FileLoader fileLoader;

	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_58, mD3D, text);
	text.setFillColor(GC::UI_COLOR);
	text.setOrigin(0, 0);
	text.setString(fetchTimerText());
	text.setPosition(GC::SCREEN_RES.x * 0.770f, GC::SCREEN_RES.y * 0.015f);



	timerClock.restart();
}

int Timer::getHours()						// returns timer.hours as an int
const {
	return hours;
}

int Timer::getMinutes()						// returns timer.minutes as an int
const {
	return minutes;
}

int Timer::getSeconds()						// returns timer.seconds as an int
const {
	return seconds;
}

int Timer::getTime()						// returns timer.timerClock as an int
{
	int timeInSeconds = int(timerClock.getElapsedTime());
	return timeInSeconds;
}

void Timer::setTimer(int h, int m, int s) {	// sets the timer's time
	hours = h;
	minutes = m;
	seconds = s;
}

void Timer::update()
{
	int timeInSeconds = int(timerClock.getElapsedTime());
	hours = (timeInSeconds / 3600);
	minutes = ((timeInSeconds / 60) % 60);
	seconds = (timeInSeconds % 60);
	text.setString(fetchTimerText());
}

//////// Private: ////////

long long Timer::toSeconds()				// return time in seconds
const {
	int hold = (hours * 3600) + (minutes * 60) + seconds;	// hold variable to prevent overflow
	return hold;
}

std::string Timer::singleDigitFormat(int number) // returns a single digit numbers in a double digit format
{
	std::stringstream formattedNumber;
	if (number <= 9)
	{
		formattedNumber << "0" << number;
	}
	else
		formattedNumber << number;
	return formattedNumber.str();
}

std::string Timer::fetchTimerText()				// fetches time as text (00:00:00)
{
	std::stringstream ss;
	ss << singleDigitFormat(hours) << ":" << std::setw(2) << singleDigitFormat(minutes) << ":" << singleDigitFormat(seconds);
	std::string string(ss.str());
	return string;
}
//------------------Timer Class Functions End//