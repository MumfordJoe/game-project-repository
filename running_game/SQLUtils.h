#pragma once

#include <assert.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

#include "sqlite3.h"
#include "MyDB.h"
#include "utilities.h"
#include "gameConst.h"

using namespace std;

struct Metrics {
	const std::string VERSION = GC::SCOREBOARD_VERSION;
	int score;
	int sessions = 1;
	std::string name;
	bool useDB = true;
	MyDB db;

	struct PlayerData {
		std::string name;
		int score;
		int sessions = 1;
		int playerID = 0;
		bool oldScore = false;
	};
	std::vector<PlayerData> playerData;
	std::string filePath;

	void Restart();
	bool IsScoreInTopTen();
	void SortAndUpdatePlayerData();
	bool Save(const std::string& path = "") {
		return (useDB) ? DBSave(path) : FileSave(path);
	}
	bool Load(const std::string& path, bool _useDB) {
		useDB = _useDB;
		return (useDB) ? DBLoad(path) : FileLoad(path);
	}

	bool FileSave(const std::string& path = "");
	bool FileLoad(const std::string& path);
	bool DBSave(const std::string& path = "");
	bool DBLoad(const std::string& path);
	bool DBClose();
};

