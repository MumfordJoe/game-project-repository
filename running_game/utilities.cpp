// Accompanying Header
#include "utilities.h"

// Include Libraries
//#include "SFML/Graphics.hpp"
#include <windows.h>
#include <assert.h>
#include <string>
#include <sstream>
#include <algorithm>
#include "TexCache.h"
#include <stdlib.h>

//------------------Debug Functions Start//
void PrintToOutput(const std::string& msg1, const std::string& msg2)
{
	// First Message
	OutputDebugString(msg1.c_str());
	// Second Message (Optional)
	OutputDebugString("\n");
	if (!msg2.empty()) {
		OutputDebugString(msg2.c_str());
		OutputDebugString("\n");
	}
}

float clamp(float num, float lower, float upper)
{
	//return std::max(lower, std::min(num, upper));
	return std::clamp(num, lower, upper);
}

void encryptDecrypt(std::string& toEncrypt, char key) // Encrypts and decrypts a string using a key
{
	std::string output = toEncrypt;

	for (int i = 0; i < toEncrypt.size(); i++)
		output[i] = toEncrypt[i] ^ key;

	toEncrypt = output;
}

void moduloEncrypt(std::string& toEncrypt, char key) // Encrypts a lowercase string within the limits of the alphabet using a key
{
	std::string output = toEncrypt;

	for (int i = 0; i < toEncrypt.size(); i++)
	{
		int ascii = static_cast<int>(output[i]) + key;

		ascii = ((ascii - 'a') % 26) + 'a';			// Encryption is done here

		output[i] = static_cast<char>(ascii);
	}

	toEncrypt = output;
}

void moduloEncryptInt(int& toEncrypt, char key) // Encrypts a integer within the limits of the alphabet using a key
{
	int output = toEncrypt;

	//for (int i = 0; i < toEncrypt.size(); i++)
	//{
	//	int ascii = static_cast<int>(output[i]) + key;

	//	ascii = ((ascii - 'a') % 26) + 'a';			// Encryption is done here

	//	output[i] = static_cast<char>(ascii);
	//}

	toEncrypt = output;
}

void moduloDecrypt(std::string& toEncrypt, char key) // Decrypts a lowercase string encrypted within the limits of the alphabet using it's key [Currently doesn't work]
{
	std::string output = toEncrypt;

	for (int i = 0; i < toEncrypt.size(); i++)
	{
		int ascii = static_cast<int>(output[i]);

		ascii = ((ascii - (key - 'a')) % 26) + 'a' - 12;	// Decryption is done here

		output[i] = static_cast<char>(ascii);
	}

	toEncrypt = output;
}

void moduloDecryptInt(int& toEncrypt, char key) // Decrypts a integer encrypted using it's key [Currently doesn't work]
{
	int output = toEncrypt;

	//for (int i = 0; i < toEncrypt.size(); i++)
	//{
	//	int ascii = static_cast<int>(output[i]);

	//	ascii = ((ascii - (key - 'a')) % 26) + 'a' - 12;	// Decryption is done here

	//	output[i] = static_cast<char>(ascii);
	//}

	toEncrypt = output;
}
//------------------Debug Functions End//

//------------------Collider Class Functions Start//
//////// Public: ////////

bool Collider::isColliding(Sprite circleObject, Sprite rectangleObject, bool firstIsRect, bool secondIsCircle)		// Returns true if the two given sprite objects are colliding
{
	DirectX::SimpleMath::Vector2 circlePos = circleObject.getPosition();
	DirectX::SimpleMath::Vector2 rectanglePos = rectangleObject.getPosition();
	
	DirectX::SimpleMath::Vector2 circleScale = rectangleObject.getScale();
	DirectX::SimpleMath::Vector2 rectScale = rectangleObject.getScale();
	
	RECTF circleRect = circleObject.getTextureRect();
	float circleRect_width = circleRect.right - circleRect.left;
	float circleRect_height = circleRect.bottom - circleRect.top;
	circleRect_width *= circleScale.x;
	circleRect_height *= circleScale.y;
	float circleRadius = (circleRect.bottom - circleRect.top) / 2;
	circleRadius *= circleScale.y;
	
	RECTF rectRect = rectangleObject.getTextureRect();
	float rectRect_width = rectRect.right - rectRect.left;
	float rectRect_height = rectRect.bottom - rectRect.top;
	rectRect_width *= rectScale.x;
	rectRect_height *= rectScale.y;
	float rectRadius = (rectRect.bottom - rectRect.top) / 2;
	rectRadius *= rectScale.y;

	float circleDistanceX = abs(circlePos.x - rectanglePos.x);
	float circleDistanceY = abs(circlePos.y - rectanglePos.y);


	if (firstIsRect)
	{
		if (secondIsCircle)	// If the first is a rectangle and the second is a circle
		{
			assert(false);
		}
		else				// If both are rectangles
		{
			if (((circlePos.x + circleRect_width) > rectanglePos.x && ((circlePos.x) < (rectanglePos.x + rectRect_width)))
				&& ((circlePos.y + circleRect_height) > rectanglePos.y && ((circlePos.y) < (rectanglePos.y + rectRect_height))))
			{
				return true;
			}
			return false;
		}
	}
	else
	{
		if (secondIsCircle)	// If both are circles
		{
			if (((circlePos.x + circleRadius) > (rectanglePos.x - rectRadius) && ((circlePos.x - circleRadius) < (rectanglePos.x + rectRadius)))
				&& ((circlePos.y + circleRadius) > (rectanglePos.y - rectRadius) && ((circlePos.y - circleRadius) < (rectanglePos.y + rectRadius))))
			{
				return true;
			}
			return false;
		}
		else				// If the first is a circle and the second is a rectangle
		{
			if (((circlePos.x + circleRadius) > rectanglePos.x && ((circlePos.x - circleRadius) < (rectanglePos.x + rectRect_width)))
				&& ((circlePos.y + circleRadius) > rectanglePos.y && ((circlePos.y - circleRadius) < (rectanglePos.y + rectRect_height))))
			{
				return true;
			}
			return false;
		}
	}

}
//------------------Collider Class Functions End//

//------------------File Loader Class Functions Start//
//////// Public: ////////

ID3D11ShaderResourceView* FileLoader::loadTexture(const std::string& texLocation, MyD3D& d3d)
{
	//DirectX::DDS_ALPHA_MODE alpha;
	//ID3D11ShaderResourceView* pT = nullptr;
	//if (DirectX::CreateDDSTextureFromFile(&pDevice, texLocation.c_str(), nullptr, &pT, 0, &alpha) != S_OK)
	//{
	//	WDBOUT(L"Cannot load " << texLocation << L"\n");
	//	assert(false);
	//}
	//assert(pT);
	//return pT;
	ID3D11ShaderResourceView* pT = d3d.GetCache().LoadTexture(&d3d.GetDevice(), texLocation, texLocation);
	return pT;
}

SpriteFont* FileLoader::loadFont(const std::wstring& fileLocation, MyD3D& d3d)
{
	SpriteFont* font = nullptr;

	//Convert the std::wstring into a const wchar_t
	font = new SpriteFont(&d3d.GetDevice(), fileLocation.c_str());
	//font = new SpriteFont(&d3d.GetDevice(), fileLocation.c_str());
	return font;
}

void FileLoader::assignTexToSpr(const std::string& texLocation, MyD3D& d3d, Sprite& spr)
{
	ID3D11ShaderResourceView* texture = loadTexture(texLocation, d3d);
	spr.setTexture(texture);
}

void FileLoader::assignFontToText(std::wstring fontLocation, MyD3D& d3d, Text& text)
{
	SpriteFont* font = loadFont(fontLocation, d3d);
	text.setFont(font);
}

HICON FileLoader::loadIcon(HINSTANCE hInstance)
{
	return static_cast<HICON>(::LoadImage(hInstance, MAKEINTRESOURCE(IDI_MYICON), IMAGE_ICON, 48, 48, LR_DEFAULTCOLOR));
}

//------------------File Loader Class Functions End//