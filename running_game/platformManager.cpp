// Accompanying Header
#include "platformManager.h"

// Include Libraries
#include <cmath>

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Platform Manager Class Functions Start//
//////// Public: ////////

PlatformManager::PlatformManager(MyD3D& d3d)
	: mD3D(d3d)
{
	init();
}

PlatformManager::~PlatformManager()
{
	clearPlatforms();
}

void PlatformManager::init()
{
	platformSpawnTimer = maxPlatformSpawnTimerLength;
	clearPlatforms();
}

void PlatformManager::spawnPlatform()
{
	platform_instances.push_back(new Platform(mD3D));
}

void PlatformManager::spawnPlatform(float xPos, float yPos, float minScale, float maxScale, bool randScale)
{
	platform_instances.push_back(new Platform(xPos, yPos, minScale, maxScale, randScale, mD3D));
	yPos = lastSpawnedPlatformsHeight;
}

// Platform Spawning Code
void PlatformManager::spawnTimer(float elapsed, float environmentSpeed)
{

	if (platformSpawnTimer <= 0)
	{
		float xPos = RandomNumberGenerator::getRandomFloat(minPlatformXPosition, maxPlatformXPosition);
		float yPos = RandomNumberGenerator::getRandomFloat(minPlatformYPosition, maxPlatformYPosition);
		clamp(yPos, minPlatformYPosition, lastSpawnedPlatformsHeight);

		spawnPlatform(xPos, yPos, minPlatformWidth, maxPlatformWidth, true);

		platformSpawnTimer = RandomNumberGenerator::getRandomFloat(minPlatformSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed),maxPlatformSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed));

		//platformSpawnTimer = 60; //test
	}
	else
	{
		platformSpawnTimer -= elapsed;

		if (platformSpawnTimer < 0)
			platformSpawnTimer = 0;
	}
}

void PlatformManager::checkDeletePlatform(Platform* platform, int i)
{
	DirectX::SimpleMath::Vector2 pos = platform->spr.getPosition();
	float width = (platform->spr.getTextureRect().right - platform->spr.getTextureRect().left) * platform->spr.getScale().x;
	if ((pos.x + width) < GC::SCREEN_RES.x * -0.5) //-100
	{
		platform_instances.erase(platform_instances.begin() + i);
		platform->deleteInstance();
	}
}

void PlatformManager::clearPlatforms()
{
	int platformNo = platform_instances.size();
	for (int i = 0; i < platformNo; i++)
	{
		platform_instances[i]->deleteInstance();
	}
	platform_instances.clear();
}

void PlatformManager::destroy()
{
	delete this;
}
//------------------Platform Manager Class Functions End//