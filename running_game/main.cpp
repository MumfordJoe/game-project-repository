////////////////////////////////////////////////////////////////
// 3DGD Game Project: A 3D DX11 Game //// Endless Runner Game //
////////////////////////////////////////////////////////////////

////------------------Include Libraries Start//
//// Include Libraries
//#include <iomanip>
//
//
//
////------------------Include Libraries End//
//
////------------------Main Start//
//int main()
//{
//	GameApp gameApp;
//	gameApp.initGameApp();
//
//	gameApp.window.display();
//
//	// Render game loop
//	while (gameApp.window.isOpen())
//	{
//		gameApp.renderGameApp();
//	}
//
//	return EXIT_SUCCESS;
//}
////------------------Main End//

// New Main Content from DX11

//------------------Include Libraries Start//
#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <iomanip>
#include <vector>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "Input.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "DDSTextureLoader.h"
#include "CommonStates.h"

#include <chrono>

#include "gameApp.h"
#include "eGameMode.h"

#include "gameConst.h"
#include "utilities.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;
const int ASCII_ESC = 27;
//------------------Include Libraries End//

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

int GetTimeInMilliseconds()
{
	time_t now = time(nullptr);
	time_t mnow = now * 1000;

	return mnow;
}

// MouseAndKeys Init
MouseAndKeys mouseAndKeys;

FileLoader fileLoader;
HICON ICON;

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
	// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case ASCII_ESC:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	case WM_INPUT:
		mouseAndKeys.MessageEvent((HRAWINPUT)lParam);
		break;
	case WM_CREATE:
		SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)ICON);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
	PSTR cmdLine, int showCmd)
{
	ICON = fileLoader.loadIcon(hInstance);

	//int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(GC::SCREEN_RES.x, GC::SCREEN_RES.y, hInstance, GC::GAME_NAME, MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil& wu = WinUtil::Get();
	wu.SetD3D(d3d);

	mouseAndKeys.Initialise(wu.GetMainWnd(), true, false);

	GameApp gameApp(d3d);
	gameApp.initGameApp();

	bool canUpdateRender;
	float dTime = 0;
	while (wu.BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			if (dTime < (1000 / GC:: FRAMERATE))
			{
				Sleep((1000 / GC::FRAMERATE) - dTime);
			}

			// Render Game
			gameApp.renderGameApp(mouseAndKeys, dTime);

			// Exits program when game ends
			if (gameApp.getGameMode() == EGameMode::ExitGame)
				break;
		}
		dTime = wu.EndLoop(canUpdateRender);
	}

	//ReleaseGame();
	d3d.ReleaseD3D(true);	
	return 0;
}