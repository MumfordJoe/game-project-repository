#pragma once

// Class Headers
#include "obstacleManager.h"
#include "sfxManager.h"

// Misc. Headers
#include "varTypes.h"
#include "utilities.h"
#include "gameConst.h"

class Enemy
{
public:
	Enemy(MyD3D& d3d);																									// Constructor
	Enemy(float xPos, float yPos, MyD3D& d3d);																			// Copy Constructor for spawning at a certain position

	void move(float elapsed);																							// Moves the crate to the left at a constant speed
	void attackTimer(float elapsed, float environmentSpeed, ObstacleManager& obstacleManager, SFXManager& sfxManager);
	void updateSpeed(float speedIn);
	//void collide(float elapsed, Sprite collidingSpr, bool isPlatform);												// Handles horizontal collision between the crate and the player
	void deleteInstance();																								// Calls the instance of the class to delete itself.

	MyD3D& mD3D;
	Sprite spr;																											// Enemy's Sprite
	int currentHealth;																									// Init currentHealth value in constructor / init (as setting here would have boss class as currentHealth = 1).
	bool isOnGround = false;
	bool isColliding = false;																							// Whether the crate is colliding with another object or not
	bool deleteMe = false;
	// float attackTimer = 0;

protected:																												// The only other object that can access this is the Boss class below.
	Collider collider;
	float crateAttackTimer = 0;
	float hoveringCenter = 0;
	float hoverValue = 0;
	float hoverSpeed_Current = 0;
	float hoverSpeed_Max = GC::ENEMY_HOVERINGSPEED;
	const float hoverAcceleration = GC::ENEMY_HOVERINGACCELERATION;
	bool isHovering = false;
private:
	float speed = GC::ENEMY_SPEED;																						// Use a sin wave alongside movement for effect.
	const int maxHealth = GC::ENEMY_HEALTH;																				// The maximum / starting health of the enemy.
	const float attackTimer_Max = GC::ENEMY_ATTACKTIMER_MAX;
	const float attackTimer_Min = GC::ENEMY_ATTACKTIMER_MIN;
	const float crateScale = 1;
};

class Boss : public Enemy
{
public:
	Boss(MyD3D& d3d);																									// Constructor
	Boss(float xPos, float yPos, MyD3D& d3d);																			// Copy Constructor for spawning at a certain position

	float hoverSpeed_Max = GC::BOSS_HOVERINGSPEED;
private:
	float speed = GC::BOSS_SPEED;																						// Use a sin wave alongside movement for effect.
	const int maxHealth = GC::BOSS_HEALTH;
	const float attackTimer_Max = GC::BOSS_ATTACKTIMER_MAX;
	const float attackTimer_Min = GC::BOSS_ATTACKTIMER_MIN;
	const float crateScale = 1.75;
};