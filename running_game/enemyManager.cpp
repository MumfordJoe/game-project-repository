// Accompanying Header
#include "enemyManager.h"

// Include Libraries
#include <cmath>

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Enemy Manager Class Functions Start//
//////// Public: ////////

EnemyManager::EnemyManager(MyD3D& d3d)
	: mD3D(d3d)
{
	init();
}

EnemyManager::~EnemyManager()
{
	clearEnemies();
	clearBosses();
}

void EnemyManager::init()
{
	enemySpawnTimer = maxEnemySpawnTimerLength;
	bossSpawnTimer = maxBossSpawnTimerLength;
	clearEnemies();
	clearBosses();
}

void EnemyManager::spawnEnemy()
{
	enemy_instances.push_back(new Enemy(mD3D));
}

void EnemyManager::spawnEnemy(float xPos, float yPos)
{
	enemy_instances.push_back(new Enemy(xPos, yPos, mD3D));
}

void EnemyManager::spawnBoss()
{
	boss_instances.push_back(new Boss(mD3D));
}

void EnemyManager::spawnBoss(float xPos, float yPos)
{
	boss_instances.push_back(new Boss(xPos, yPos, mD3D));
}

// Platform Spawning Code
void EnemyManager::spawnTimer(float elapsed, float environmentSpeed, int difficultyLevel)
{
	if ((enemySpawnTimer <= 0) && difficultyLevel >= GC::ENEMY_SPAWNATLEVEL)
	{
		float xPos = RandomNumberGenerator::getRandomFloat(minEnemyXPosition, maxEnemyXPosition);
		float yPos = RandomNumberGenerator::getRandomFloat(minEnemyYPosition, maxEnemyYPosition);

		spawnEnemy(xPos, yPos);

		enemySpawnTimer = RandomNumberGenerator::getRandomFloat(minEnemySpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed), maxEnemySpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed));
	}
	else
	{
		enemySpawnTimer -= elapsed;;

		if (enemySpawnTimer < 0)
			enemySpawnTimer = 0;
	}
	if ((bossSpawnTimer <= 0) && difficultyLevel >= GC::BOSS_SPAWNATLEVEL)
	{
		float xPos = RandomNumberGenerator::getRandomFloat(minBossXPosition, maxBossXPosition);
		float yPos = RandomNumberGenerator::getRandomFloat(minBossYPosition, maxBossYPosition);

		spawnBoss(xPos, yPos);

		bossSpawnTimer = RandomNumberGenerator::getRandomFloat(minBossSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed), maxBossSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed));
	}
	else
	{
		bossSpawnTimer -= elapsed;;

		if (bossSpawnTimer < 0)
			bossSpawnTimer = 0;
	}
}

void EnemyManager::checkDeleteEnemies(Enemy* enemy, int i, SFXManager& sfxManager)
{
	DirectX::SimpleMath::Vector2 pos = enemy->spr.getPosition();
	float width = (enemy->spr.getTextureRect().right - enemy->spr.getTextureRect().left) * enemy->spr.getScale().x;
	if ((pos.x + width) < GC::SCREEN_RES.x * -0.5) //-100
	{
		enemy_instances.erase(enemy_instances.begin() + i);
		enemy->deleteInstance();
	}

	if (enemy->deleteMe == true)
	{
		enemy_instances.erase(enemy_instances.begin() + i);
		sfxManager.playSFX(8);
		enemy->deleteInstance();
	}

	// Check if enemy is dead
	if (enemy->currentHealth <= 0)
	{
		enemy_instances.erase(enemy_instances.begin() + i);
		sfxManager.playSFX(8);
		enemy->deleteInstance();
	}
}

void EnemyManager::checkDeleteBosses(Boss* boss, int i, SFXManager& sfxManager)
{
	DirectX::SimpleMath::Vector2 pos = boss->spr.getPosition();
	float width = (boss->spr.getTextureRect().right - boss->spr.getTextureRect().left) * boss->spr.getScale().x;
	if ((pos.x + width) < GC::SCREEN_RES.x * -0.5) //-100
	{
		boss_instances.erase(boss_instances.begin() + i);
		boss->deleteInstance();
	}

	if (boss->deleteMe == true)
	{
		boss_instances.erase(boss_instances.begin() + i);
		sfxManager.playSFX(8);
		boss->deleteInstance();
	}

	// Check if enemy is dead
	if (boss->currentHealth <= 0)
	{
		boss_instances.erase(boss_instances.begin() + i);
		sfxManager.playSFX(8);
		boss->deleteInstance();
	}
}

void EnemyManager::clearEnemies()
{
	int enemyNo = enemy_instances.size();
	for (int i = 0; i < enemyNo; i++)
	{
		enemy_instances[i]->deleteInstance();
	}
	enemy_instances.clear();
}

void EnemyManager::clearBosses()
{
	int bossNo = boss_instances.size();
	for (int i = 0; i < bossNo; i++)
	{
		boss_instances[i]->deleteInstance();
	}
	boss_instances.clear();
}

void EnemyManager::destroy()
{
	delete this;
}
//------------------Enemy Manager Class Functions End//