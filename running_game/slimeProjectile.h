#pragma once

// Include Libraries
#include "crate.h"
#include "platform.h"
#include "enemies.h"

// Misc. Headers
#include "gameConst.h"
#include "utilities.h"

//------------------Projectile Class Declaration Start//
class Projectile
{
public:
	Projectile(MyD3D& d3d, Sprite player);																							// Constructor
	Projectile(float xPos, float yPos, MyD3D& d3d, Sprite player);																	// Copy Constructor for spawning at a certain position

	void move(float elapsed);																					// Moves the projectile to the right with an arc
	void gravity(float elapsed, DirectX::SimpleMath::Vector2 screenSz);
	bool collide(float elapsed, Crate* crate);																	// Handles collision between the projectile and crates
	void collide(float elapsed, Platform* platform);															// Handles collision between the projectile and platforms
	void collide(float elapsed, Enemy* enemy, int& playerKills);												// Handles collision between the projectile and enemies
	void collide(float elapsed, Boss* boss, int& playerKills, SFXManager& sfxManager);							// Handles collision between the projectile and bosses
	void deleteInstance();																						// Calls the instance of the class to delete itself.

	MyD3D& mD3D;
	Sprite spr;																									// Projectile's Sprite
	bool isColliding = false;																					// Whether the projectile is colliding with another object or not
	bool deleteMe = false;																						// When true, this instance is deleted by the player
private:
	float gravity_current = GC::PROJECTILE_GRAVITY;																// The gravity of the projectile
	float speed = GC::PROJECTILE_SPEED;																			// The speed of the projectile
	Collider projectileCollider;																				// The projectile object's collider
};
//------------------Projectile Class Declaration Start//