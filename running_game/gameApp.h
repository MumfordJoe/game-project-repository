#pragma once

// Include Libraries
#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include <SpriteBatch.h>
//#include "SFML/Graphics.hpp"

// Class Headers
#include "game.h"
#include "intro.h"
#include "outro.h"
#include "musicManager.h"
#include "sfxManager.h"

// Misc. Headers
#include "eGameMode.h"

//------------------GameApp Class Declaration Start//
class GameApp
{
public:
	GameApp(MyD3D& d3d);											// Constructor

	void initGameApp();												// Starts the game app / program
	void renderGameApp(MouseAndKeys& mouseAndKeys, float dTime);	// Runs / renders the game app / program
	void exitGameApp();												// Closes the game app / program
	EGameMode getGameMode();										// Returns the mode of the game (returns _eGameMode)
	void setGameMode(EGameMode newMode);							// Sets the mode of the game
	
	DirectX::SpriteBatch* pBatch = nullptr;
	MyD3D& mD3D;

private:
	Game game;
	MainMenu menu;
	Scoreboard scoreboard;
	MusicManager musicManager;
	SFXManager sfxManager;
	bool menuInit = false;
	bool gameInit = false;
	bool scoreboardInit = false;
	int difficulty = 1;												// Holds the diffculty of the game the player wants to play, it is normal mode by default (1)
	int gameScore = 0;												// Holds the score of the most recent game session to add to the scoreboard

	EGameMode _eGameMode;											// The variable that holds the mode of the game, part of the state machine for game modes
};
//------------------GameApp Class Declaration End//
