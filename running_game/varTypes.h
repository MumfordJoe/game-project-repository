#pragma once

// Include Headers
#include "SpriteBatch.h"
#include "SpriteFont.h"
#include "CommonStates.h"
#include <sstream>
#include <iomanip>
#include "WindowUtils.h"
#include <ctime>

// DX11 Headers
#include "D3D.h"
#include "D3DUtil.h"
#include "Input.h"
#include "D3d9types.h"

using namespace DirectX;

//------------------RECTF Struct Declaration Start//
struct RECTF
{
	float left, top, right, bottom;
	operator RECT() {
		return RECT{ (int)left,(int)top,(int)right,(int)bottom };
	}
};
//------------------RECTF Struct Declaration End//

//------------------Text Class Declaration Start//
class Text
{
public:
	Text(MyD3D& d3d);											// Text Constructor

	void draw(DirectX::SpriteBatch& batch);						// Draws the text to window

	// Getters
	const DirectX::SimpleMath::Vector2 getPosition() const;
	const float getRotation() const;
	const float getScale() const;
	const DirectX::SimpleMath::Vector2 getOrigin() const;

	const RECT getGlobalBounds()
	{
		std::wstring widestr = std::wstring(string.begin(), string.end());
		const wchar_t* widecstr = widestr.c_str();

		RECT rect = textFont->MeasureDrawBounds(widecstr, DirectX::XMFLOAT2(0.f, 0.f));

		return rect;
	}

	// Setters
	void setString(std::string stringIn);
	void setFont(SpriteFont* font);
	void setPosition(const DirectX::SimpleMath::Vector2& posIn);
	void setPosition(const float& posInX, const float& posInY);
	void setRotation(const float rotationIn);
	void setScale(const float& scaleIn);
	void setOrigin(const DirectX::SimpleMath::Vector2& originIn);
	void setOrigin(const float& originInX, const float& originInY);
	void setFillColor(DirectX::SimpleMath::Vector4 colorIn);

	//void setScale(const float& scaleInX, const float& scaleInY)
	//{
	//	scale.x = scaleInX;
	//	scale.y = scaleInY;
	//}

	//void setFillColor(Color colorIn)
	//{
	//	color.x = colorIn.r;
	//	color.y = colorIn.g;
	//	color.z = colorIn.b;
	//	color.w = colorIn.a;
	//}

private:
	DirectX::SpriteFont* textFont;
	std::string string;
	DirectX::SimpleMath::Vector2 pos;
	float scale;
	DirectX::SimpleMath::Vector2 origin;
	DirectX::SimpleMath::Vector4 color;
	float rotation;
	float depth;

	MyD3D& mD3D;
};
//------------------Text Class Declaration End//

//------------------Sprite Class Declaration Start//
class Sprite
{
public:
	Sprite(MyD3D& d3d);

	Sprite(const Sprite& rhs)
		:mD3D(rhs.mD3D)
	{
		(*this) = rhs;
	}
	Sprite& operator=(const Sprite& rhs);

	void draw(DirectX::SpriteBatch& batch);													// Sprite is drawn using batch parameter

	void rotate(float degreesIn)
	{
		rotation -= degreesIn;
	}

	// Getters
	const DirectX::SimpleMath::Vector2 getPosition() const;
	const float getRotation() const;
	const DirectX::SimpleMath::Vector2 getScale() const;
	const DirectX::SimpleMath::Vector2 getOrigin() const;
	const RECTF getTextureRect() const;
	const TexCache::Data& getTexData() const;
	ID3D11ShaderResourceView& getTex();
	const DirectX::SimpleMath::Vector4 getColor();
	DirectX::SimpleMath::Vector2 GetScreenSize() const {
		assert(mpTexData);
		return scale * mpTexData->dim;
	}

	// Setters
	void setPosition(const DirectX::SimpleMath::Vector2& posIn);
	void setPosition(const float& posInX, const float& posInY);
	void setRotation(const float rotationIn);
	void setScale(const DirectX::SimpleMath::Vector2& scaleIn);
	void setScale(const float& scaleInX, const float& scaleInY);
	void setOrigin(const DirectX::SimpleMath::Vector2& originIn);
	void setOrigin(const float& originInX, const float& originInY);
	//void setTextureRect(const RECT rectIn);
	void setTextureRect(const float left, const float top, const float right, const float bottom);
	void setTextureRect(const RECTF& texRect);
	//void setTexture(Texture& tex);
	//void setTexture(Texture& tex, const RECT& texRect = RECT());
	void setTexture(ID3D11ShaderResourceView& tex, const RECTF& texRect = RECTF{ 0,0,0,0 });
	void setTexture(ID3D11ShaderResourceView* tex, const RECTF& texRect = RECTF{ 0,0,0,0 });
	void setColor(DirectX::SimpleMath::Vector4 colorIn);
	void setColor(float r, float g, float b, float a);

private:

	MyD3D& mD3D;

	RECTF mTexRect;
	const TexCache::Data* mpTexData;
	ID3D11ShaderResourceView* mTexture;
	DirectX::SimpleMath::Vector2 pos;
	DirectX::SimpleMath::Vector2 vel;
	DirectX::SimpleMath::Vector2 scale;
	DirectX::SimpleMath::Vector2 origin;
	DirectX::SimpleMath::Vector4 colour;
	float rotation;
	float depth;
};
//------------------Sprite Class Declaration End//

//------------------Clock Class Declaration Start//
class Clock 
{
public:
	Clock()
	{
		time_t now = time(0);
		seconds = 0;
		startingTime = now;
	}
	int getElapsedTime()
	{
		update();
		return seconds;
	}
	void restart()
	{
		time_t now = time(0);
		seconds = 0;
		startingTime = now;
	}
private:
	void update()
	{
		time_t now = time(0);
		seconds = now - startingTime;
	}
	time_t seconds;
	time_t startingTime;
};
//------------------Clock Class Declaration End//