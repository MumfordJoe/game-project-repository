#pragma once

// Class Headers
#include "sfxManager.h"

// Misc. Headers
#include "gameConst.h"
#include "utilities.h"
#include "varTypes.h"

//------------------Player Class Declaration Start//
class Player
{
public:
	Player(MyD3D& d3d);																							// Constructor
	//~Player();

	void init();
	void jump(float elapsed, DirectX::SimpleMath::Vector2 screenSz, bool smallJump, SFXManager& sfxManager);	// Makes the player object jump vertically
	void attack(SFXManager& sfxManager);																		// Makes the player object attack the area in front of itself
	void gravity(float elapsed, DirectX::SimpleMath::Vector2 screenSz);											// Put the player under the affects of gravity
	void collide(float elapsed, Sprite spr);																	// Handles vertical collision between the player and platforms
	void changeActiveSpriteTo(int sprRefNo);																	// Changes the player's currently active sprite (such as a jumping or attacking sprite)
	void recover(float elapsed, DirectX::SimpleMath::Vector2 screenSz);											// Moves the player rightward if they have been pushed leftward too far by platforms
	void animate(float elapsed);																				// Animates the player sprite
	void updateEnvironmentSpeedCopy(float environmentSpeedIn);
	void giveCoins(int amount);

	Sprite spr_active;																							// Player's Current Sprite
	
	bool isOnGround = false;																					// Whether the player is touching the ground or not
	bool canJump = false;																						// Whether the player can jump
	bool isColliding = false;																					// Whether the player is colliding with another object or not
	bool isColliding_Horizontally = false;																		// Whether the player is colliding horizontally with another object or not
	bool dead = false;																							// Whether the player has lost the game or not
	
	int playerCoins = 0;																						// The number of coins the player has collected
	int playerKills = 0;																						// The number of enemies the player has defeated
	Text playerCoinText;
	Sprite coinSprite_active;																					// The sprite for the coin next to the amount of coins displayed

private:
	MyD3D& mD3D;

	Sprite spr_run;																								// Player's Running Sprite
	Sprite spr_jump;																							// Player's Jumping Sprite
	Sprite spr_fall;																							// Player's Falling Sprite
	Sprite spr_attack;																							// Player's Attacking Sprite
	Sprite spr_cling;																							// Player's Wall-Clinging Sprite
	int currentSprite;																							// Holds a number that refers to the sprite being used in spr_active
	float attackAnimationTimer = 0;																				// How long until the player's attacking animation stops
	float attackAnimationLength = GC::PLAYER_ATTACKANIMATIONLENGTH;												// The length the attacking sprite is shown, in seconds

	float speed_recovery = GC::PLAYER_SPEED;																	// Player's Default Speed
	float speed = 0;																							// Player's Current Speed
	float rotationSpeed = GC::PLAYER_ROLLSPEED;																	// The degree of rotation the player rotates every frame
	float gravity_current = GC::PLAYER_GRAVITY;																	// How much the player is currently affected by gravity's pull
	float environmentSpeedCopy = GC::ENVIRONMENTOBJECT_SPEED;
	
	Collider playerCollider;																					// The player object's collider
	
	float startingPosX = GC::PLAYER_STARTINGPOSX;																// The x position that player begins the game at

	float coinSpin = 0;																							// Increments and spins the coin via changing it's x scale.
	float originalXScale;																						// Used to store the coin's scale in the x direction when initalised.d.
	Sprite coinSprite;																							// The sprite for the coin next to the amount of coins displayed
};
//------------------Player Class Declaration Start//