#pragma once

// Class Headers
#include "slimeProjectile.h"
#include "player.h"
#include "sfxManager.h"

// Misc Headers
#include "utilities.h"
#include "varTypes.h"

//------------------Obstacle Manager Class Declaration Start//
class ProjectileManager
{
public:
	ProjectileManager(MyD3D& d3d);								// Constructor
	~ProjectileManager();										// Destructor

	void init();
	void spawnProjectile(Player player, SFXManager& sfxManager);
	void projectileTimer(float elapsed);
	void checkDeleteProjectile(Projectile* projectile, int i);
	void clearProjectiles();
	void destroy();												// Deletes this instance of ProjectileManager



	std::vector < Projectile* > projectile_instances{};			// Holds and points to all of the enemies that have been created through spawnPEnemy()
	float projectileCooldown = GC::PROJECTILE_COOLDOWN;			// The time until the next enemy is spawned

private:
	MyD3D& mD3D;
};
//------------------Obstacle Manager Class Declaration End//