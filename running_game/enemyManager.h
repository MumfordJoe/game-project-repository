#pragma once

// Class Headers
#include "enemies.h"

// Misc Headers
#include "utilities.h"
#include "varTypes.h"

//------------------Enemy Manager Class Declaration Start//
class EnemyManager
{
public:						
	EnemyManager(MyD3D& d3d);																	// Constructor
	~EnemyManager();																			// Destructor

	void init();
	void spawnEnemy();																			// Creates a enemy and adds it to the enemy_instance vector array
	void spawnEnemy(float xPos, float yPos);													// Creates a enemy at a position and adds it to the enemyinstance vector array
	void spawnBoss();																			// Creates a boss and adds it to the boss_instances vector array
	void spawnBoss(float xPos, float yPos);														// Creates a boss at a position and adds it to the boss_instances vector array
	void spawnTimer(float elapsed, float environmentSpeed, int difficultyLevel);				// Creates enemies and bosses at calculated intervals
	void checkDeleteEnemies(Enemy* enemy, int i, SFXManager& sfxManager);						// Checks a enemy's current state and deletes it when necessary
	void checkDeleteBosses(Boss* boss, int i, SFXManager& sfxManager);							// Checks a boss' current state and deletes it when necessary
	void clearEnemies();																		// Deletes all enemies and empties enemy_instances
	void clearBosses();																			// Deletes all bosses and empties boss_instances
	void destroy();																				// Deletes this instance of EnemyManager

	std::vector < Enemy* > enemy_instances{};													// Holds and points to all of the enemies that have been created through spawnPEnemy()
	float enemySpawnTimer = 0;																	// The time until the next enemy is spawned
	std::vector < Boss* > boss_instances{};														// Holds and points to all of the bosses that have been created through spawnBoss()
	float bossSpawnTimer = 0;																	// The time until the next boss is spawned

private:
	MyD3D& mD3D;

	float maxEnemySpawnTimerLength = GC::ENEMY_SPAWNTIMER_MAX;									// The maximum values for how long it takes until the next enemy is spawned
	float minEnemySpawnTimerLength = GC::ENEMY_SPAWNTIMER_MIN;									// The minimum values for how long it takes until the next enemy is spawned
	float maxEnemyXPosition = GC::ENEMY_XPOS_MAX;												// The maximum x position the enemy manager can spawn a enemy at
	float minEnemyXPosition = GC::ENEMY_XPOS_MIN;												// The minimum x position the enemy manager can spawn a enemy at
	float maxEnemyYPosition = GC::ENEMY_YPOS_MAX;												// The maximum y position the enemy manager can spawn a enemy at
	float minEnemyYPosition = GC::ENEMY_YPOS_MIN;												// The minimum y position the enemy manager can spawn a enemy at

	float maxBossSpawnTimerLength = GC::BOSS_SPAWNTIMER_MAX;									// The maximum values for how long it takes until the next boss is spawned
	float minBossSpawnTimerLength = GC::BOSS_SPAWNTIMER_MIN;									// The minimum values for how long it takes until the next boss is spawned
	float maxBossXPosition = GC::BOSS_XPOS_MAX;													// The maximum x position the boss manager can spawn a Boss at
	float minBossXPosition = GC::BOSS_XPOS_MIN;													// The minimum x position the boss manager can spawn a Boss at
	float maxBossYPosition = GC::BOSS_YPOS_MAX;													// The maximum y position the boss manager can spawn a Boss at
	float minBossYPosition = GC::BOSS_YPOS_MIN;													// The minimum y position the boss manager can spawn a Boss at
};
//------------------Enemy Manager Class Declaration End//