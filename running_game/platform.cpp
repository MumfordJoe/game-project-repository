// Accompanying Header
#include "platform.h"

// Class Headers
#include "randomNumberGenerator.h"

//------------------Platform Class Functions Start//
//////// Public: ////////

// Default Constructor
Platform::Platform(MyD3D& d3d)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLATFORM, mD3D, spr);
	//RECTF texR = spr.getTextureRect();
	spr.setOrigin(0, 10);
	float scale = RandomNumberGenerator::getRandomFloat(width_min, width_max);
	spr.setScale(scale, (scale > minHeight)?scale:minHeight);
	spr.setPosition(GC::SCREEN_RES.x * 1.15f, GC::SCREEN_RES.y * 0.665f);	// Sets the position of the object when created.
}

Platform::Platform(float xPos, float yPos, float minScale, float maxScale, bool randScale, MyD3D& d3d)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_PLATFORM, mD3D, spr);
	//RECTF texR = spr.getTextureRect();
	spr.setOrigin(0, 10);
	if (randScale == true)
	{
		spr.setScale(RandomNumberGenerator::getRandomFloat(minScale, maxScale), minHeight);
	}
	else 
	{
		spr.setScale(minScale, minHeight);
	}
	spr.setPosition(xPos, yPos);	// Sets the position of the object when created.
}

void Platform::move(float elapsed)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();
	pos.x -= speed * elapsed;
	spr.setPosition(pos);
}

void Platform::deleteInstance()
{
	delete this;
}

void Platform::updateSpeed(float speedIn)
{
	speed = speedIn;
}

void Platform::collide(float elapsed, Sprite playerSpr)
{
	DirectX::SimpleMath::Vector2 pos = playerSpr.getPosition();
	isColliding = platformCollider.isColliding(playerSpr, spr, false, false);

	if (isColliding)
	{
		//gravity_current = 0;
		pos.x -= speed * 1.25 * elapsed;
		//isOnGround = true;
	}
	else
	{
		//isOnGround = false;
	}

	playerSpr.setPosition(pos);
}

//////// Private: ////////

//sf::Vector2f Platform::getRandPos()
//{
//	sf::Vector2f randPos;
//
//
//	return randPos;
//}
//------------------Platform Class Functions End//