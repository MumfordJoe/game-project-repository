// It's collider function is run in the obstacle manager's for loop in game.cpp, similar to player's collider function in platform manager's collider function in game.cpp. The same goes for enemies and bosses.
// Store instances of this class object in a vector array in player class. They can only be fired if there is <= 1 projectiles in said array. Player has a few functions to manager projectiles, like management classes.

// Accompanying Header
#include "slimeProjectile.h"

// Include Headers
#include <cmath>

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Projectile Class Functions Start//
//////// Public: ////////

// Default Constructor
Projectile::Projectile(MyD3D& d3d, Sprite player)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_PROJECTILE, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin(float((texR.right - texR.left) / 2.f) + (texR.right / 2), float(texR.bottom - texR.top) / 2.f);
	spr.setScale(1.0f, 1.0f);
	spr.setPosition(player.getPosition().x + (GC::SCREEN_RES.x * 0.025), player.getPosition().y - (GC::SCREEN_RES.y * 0.05));	// Sets the position of the object when created.
	gravity_current = (GC::PROJECTILE_GRAVITY * -100);
}

Projectile::Projectile(float xPos, float yPos, MyD3D& d3d, Sprite player)
	: mD3D(d3d), spr(mD3D)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_PROJECTILE, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin(float((texR.right - texR.left) / 2.f) + (texR.right / 2), float(texR.bottom - texR.top) / 2.f);
	spr.setScale(1.0f, 1.0f);
	spr.setPosition(xPos, yPos);																								// Sets the position of the object when created.
	gravity_current = (GC::PROJECTILE_GRAVITY * -100);
}

// Movement Code
void Projectile::move(float elapsed)
{
	// Move Rightward
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();
	pos.x += speed * elapsed;
	spr.setPosition(pos);

	// Rotate with Arc
	float angleAdjustment = asin(gravity_current / speed);
	spr.setRotation(clamp(0 + angleAdjustment, -10, 180));

}

// Gravity Code
void Projectile::gravity(float elapsed, DirectX::SimpleMath::Vector2 screenSz)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();

	gravity_current += (GC::PROJECTILE_GRAVITY * GC::GRAVITY_ACCELERATION);

	if ((pos.y) < (screenSz.y * 1.125f))
	{
		pos.y += gravity_current * elapsed;
	}
	else
	{
		deleteMe = true;
	}

	spr.setPosition(pos);
}

// Collision Code - Crates
bool Projectile::collide(float elapsed, Crate* crate)
{
	isColliding = projectileCollider.isColliding(spr, crate->spr, false, false);

	if (isColliding)
	{
		deleteMe = true;
		crate->deleteMe = true;

		float rand = RandomNumberGenerator::getRandomFloat(0, 1);
		if (rand < 0.5)
		{
			return true;
		}
	}

	return false;
}

// Collision Code - Platforms
void Projectile::collide(float elapsed, Platform* platform)
{
	isColliding = projectileCollider.isColliding(spr, platform->spr, false, false);

	if (isColliding)
	{
		deleteMe = true;
	}
}

// Collision Code - Enemies
void Projectile::collide(float elapsed, Enemy* enemy, int& playerKills)
{
	isColliding = projectileCollider.isColliding(spr, enemy->spr, false, true);

	if (isColliding)
	{
		deleteMe = true;
		enemy->currentHealth -= 1;
		playerKills += 1;
	}
}

// Collision Code - Bosses
void Projectile::collide(float elapsed, Boss* boss, int& playerKills, SFXManager& sfxManager)
{
	isColliding = projectileCollider.isColliding(spr, boss->spr, false, true);

	if (isColliding)
	{
		deleteMe = true;
		boss->currentHealth -= 1;
		sfxManager.playSFX(11);
		playerKills += 3;
	}
}

void Projectile::deleteInstance()
{
	delete this;
}
//------------------Projectile Class Functions End//