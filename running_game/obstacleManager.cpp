// Accompanying Header
#include "obstacleManager.h"

// Include Libraries
#include <cmath>

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Obstacle Manager Class Functions Start//
//////// Public: ////////

ObstacleManager::ObstacleManager(MyD3D& d3d)
	: mD3D(d3d)
{
	init();
}

ObstacleManager::~ObstacleManager()
{
	clearObstacles();
}

void ObstacleManager::init()
{
	crateSpawnTimer = maxCrateSpawnTimerLength;
	clearObstacles();
}

void ObstacleManager::spawnCrate()
{
	crate_instances.push_back(new Crate(mD3D));
}

void ObstacleManager::spawnCrate(float xPos, float yPos, float minScale, float maxScale, bool randScale)
{
	crate_instances.push_back(new Crate(xPos, yPos, minScale, maxScale, randScale, mD3D));
}

// Crate Spawning Code
void ObstacleManager::spawnTimer(float elapsed, float environmentSpeed, Sprite platformSpr)
{

	if (crateSpawnTimer <= 0)
	{
		float xPos = RandomNumberGenerator::getRandomFloat(minCrateXPosition, maxCrateXPosition);
		float yPos = RandomNumberGenerator::getRandomFloat(minCrateYPosition, maxCrateYPosition);

		if (platformSpr.getPosition().x + xPos < GC::SCREEN_RES.x)
		{
			xPos += GC::SCREEN_RES.x / 2;
		}

		spawnCrate(platformSpr.getPosition().x + xPos, platformSpr.getPosition().y - yPos * GC::CRATE_SCALE_MAX, minCrateWidth, maxCrateWidth, true);

		crateSpawnTimer = RandomNumberGenerator::getRandomFloat(minCrateSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed), maxCrateSpawnTimerLength * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed));

		//platformSpawnTimer = 60; //test
	}
	else
	{
		crateSpawnTimer -= elapsed;;

		if (crateSpawnTimer < 0)
			crateSpawnTimer = 0;
	}
}

void ObstacleManager::checkDeleteCrate(Crate* crate, int i, SFXManager& sfxManager)
{
	DirectX::SimpleMath::Vector2 pos = crate->spr.getPosition();
	float width = (crate->spr.getTextureRect().right - crate->spr.getTextureRect().left) * crate->spr.getScale().x;
	if ((pos.x + width) < GC::SCREEN_RES.x * -0.5) //-100
	{
		crate_instances.erase(crate_instances.begin() + i);
		crate->deleteInstance();
	}

	if (crate->deleteMe == true)
	{
		crate_instances.erase(crate_instances.begin() + i);
		sfxManager.playSFX(10);
		crate->deleteInstance();
	}
}

void ObstacleManager::clearObstacles()
{
	int obstacleNo = crate_instances.size();
	for (int i = 0; i < obstacleNo; i++)
	{
		crate_instances[i]->deleteInstance();
	}
	crate_instances.clear();
}

void ObstacleManager::destroy()
{
	delete this;
}
//------------------Obstacle Manager Class Functions End//