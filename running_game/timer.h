#pragma once

// Include Libraries

// Misc Libraries
#include "varTypes.h"

//------------------Timer Class Declaration End//
class Timer {
public:
	Timer(MyD3D& d3d);									// Default Constructor
	Timer(MyD3D& d3d, const Timer&);					// Copy Constructor
	Timer(MyD3D& d3d, const int, const int, const int);	// H.M.S constructor
	Timer(MyD3D& d3d, const long);						// Seconds Constructor
	~Timer();											// Destructor

	void init();
	int getHours() const;								// Returns the variable "hours" as an int
	int getMinutes() const;								// Returns the variable "minutes" as an int
	int getSeconds() const;								// Returns the variable "seconds" as an int
	int getTime();										// Returns the total time in seconds as an int
	void setTimer(int, int, int);						// Sets the time to 3 given values
	void update();										// Updates the time within the timer, allowing it to function and display the correct time

	Text text;											// Holds the text that the timer will display
	Clock timerClock;
private:
	MyD3D& mD3D;

	long long toSeconds() const;						// Returns the time in seconds
	std::string fetchTimerText();						// Fetches a string for the timer in 00:00:00 digital clock format
	std::string singleDigitFormat(int number);			// Converts single digit numbers into a double digit format when necessary (e.g. 1 -> 01 and 2 -> 02.)

	//sf::Font font;									// Holds the font used for the timer's text
	int hours, minutes, seconds;						// Holds the timer's time in hours, minutes and seconds
	//float time_start;									// The time when the timer was created and thus started [Delete]
};
//------------------Timer Class Declaration End//