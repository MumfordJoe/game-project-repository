#pragma once
// Include Libraries
#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include <SpriteBatch.h>

// Class Headers
#include "timer.h"
#include "sfxManager.h"

// Misc. Headers
#include "eGameMode.h"
#include "varTypes.h"
#include "SQLUtils.h"
#include "typingInput.h"

//------------------Scoreboard Class Declaration Start//
class Scoreboard
{
public:
	Scoreboard(MyD3D& d3d);																														// Constructor

	void initScoreboard();																														// Initialises the scoreboard
	void initScoreboard(int newScoreIn);																										// Initialises the scoreboard with new score
	bool renderScoreboard(DirectX::SpriteBatch& batch, EGameMode& _eGameMode, MouseAndKeys& mouseAndKeys, SFXManager& sfxManager, float dTime);	// Starts and runs the scoreboard
	void destroy();																																// Deletes this instance of the scoreboard
private:
	bool scoreboardInput(std::string& newName, MouseAndKeys& mouseAndKeys, DirectX::SpriteBatch& batch, SFXManager& sfxManager);				//
	void scoreboardUpdate();

	MyD3D& mD3D;
	Metrics metrics;
	Typing typing;

	Text scoreboardTitle;
	Text scoreboardText;
	Text drawnName;
	bool keyDown = true;
	bool isNameInput = false;
	bool isScoreUpdated = false;
	int newScore = 0;
	std::string newName;
	float continueTimer = 2;
	Timer timer;
};
//------------------Scoreboard Class Declaration End//