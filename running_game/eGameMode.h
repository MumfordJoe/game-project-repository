#pragma once
//#ifndef

//------------------GameMode Class Declaration Start//
enum class EGameMode
{
	// Game-State Machine States //
	Intro,	// The main menu of the game
	Game,	// The gameplay itself
	Outro,	// The game over screen and scoreboard
	ExitGame
};
//------------------GameMode Class Declaration End//