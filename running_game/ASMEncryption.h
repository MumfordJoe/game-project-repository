#define _CRT_SECURE_NO_WARNINGS
#include <string>     // for string class
#include <chrono>     // date & time functions
#include <ctime>      // date & time string functions
#include <iostream>   // for cout <<
#include <fstream>    // for file I/O
#include <iomanip>    // for fancy output
#include <functional> // for std::reference_wrapper
#include <vector>     // for vector container class

constexpr char ENCRYPTION_KEY = 'F';                    // Encryption Key
constexpr int MAX_CHARS = 12;                           // The maximum number of characters the encrypted string can be.                            
constexpr char STRING_TERMINATOR = '$';                 // (Custom) String Terminator Character

//char original_chars[MAX_CHARS];                         // Original character string
//char encrypted_chars[MAX_CHARS];                        // Encrypted character string
//char decrypted_chars[MAX_CHARS];                        // Decrypted character string

//---------------------------------------------------------------------------------------------------------------
//----------------- ENCRYPTION ROUTINE --------------------------------------------------------------------------

void encrypt_chars (int length, char EKey, char *original_chars, char *encrypted_chars)
{
  char temp_char;             // Character Temporary Store
  int l = length;             // Length Temporary Store (This stores

    __asm
    {
      push  ecx               // Pushing the registers used in the forloop before it's execution for safety.
      push  ebx
      push  eax

      mov   ecx, 0            // Setting ecx as the forloop's counter by moving 0 into it. (Equivalent to i = 0 in a C++ forloop.)
      jmp   checkend          // Jumping to a label beyond "add ecx,1" as to not increment the forloop counter before any work has been done.

    forloop:                  // Encrypt Characters one at a time
      add   ecx, 1            // Incrementing the forloop's counter, ecx. (Equivalent to i++ in a C++ forloop.)

    checkend:
      cmp   ecx, l            // Comparing ecx, the forloop counter, to the length of the array to see whether the forloop should stop looping.
      jge   endfor            // Jumping beyond the body of the forloop to end the loop. (This and the line above is equivalent to i <= length in a C++ forloop.)

      lea   ebx, [original_chars + ecx] // Get the position of the current char from original_chars array and put it's address in ebx.
      movzx eax, byte ptr[ebx]// Move the character from ebx's address into eax and pad it with zeros to fit the correct size of eax.
      mov   temp_char, al     // Move the bits containing the character from eax into temp_char.


      push  eax               // Pushing EAX's contents onto the stack to be saved as it'll be used as an input parameter for the encryption key.
      push  ecx               // ECX is also pushed as it'll used as an input parameter for the to-be encrypted character. EDX is pushed as it'll
      push  edx               // be used as the output parameter for the encryption routine. (All of the caller registers are pushed.)
 
      movzx ecx, temp_char    // Moving the to-be encrypted character into the ECX, padding out the empty bits with 0s to allow it to fit within the register correctly.
      lea   eax, EKey         // Puts the address of the Encryption Key into the EAX register to be pushed to the stack for later use.
      push  eax               // Pushes the EAX onto the stack so it can be used as part of the base pointer code in the encryption routine later.
      push  ecx               // The same happens for ECX.

      //------------------------- Encryption Rountine - No.10 -----------------------------------
      // This code encrypts a given character (ECX) using the given encryption key from the EAX
      // register. This is intended to be repeated in a for loop to complete an entire string.
      //
      // Inputs: register   EAX = The 32-bit address of the Ekey.
      //                    ECX = The character to be encrypted (in the low 8-bit field, CL).
      // Output: register   EAX = The encrypted value of the source character (in the low 8-bit field, AL).

      push  ecx               // Pushing the to-be encrypted character to the stack to allow the use of the ECX register for other operations.

      mov   ecx, [eax]        // Moving the address of the EAX register into the ECX register to be used in the AND operation below.
      and   ecx, 0x000000A7   // Performs an AND function using ECX and the binary value of 167, the result is put into ECX to create a seemingly random value.
      rol   cl, 1             // Shifts the bits of ECX's low bit field left by one place to "randomise" the values. The furthest left value loops to become the furthest right.
      add   ecx, 0x03         // (0x00000003 = 3 = 0000 0011) Adds the binary value of 3 to the ECX register. Adding "randomness" to the value.
      mov   edx, ecx          // Moves ECX's contents into EDX, as ECX needs to be free for the upcoming code. EDX is suitable as it's being used as an output parameter.

      pop   ecx               // Popping the to-be encrypted character off of the stack back into the ECX register as it is finished being used.

      add   ecx, edx          // Adds the contents of ECX prior to the pop stack instruction to the contents after.
      ror   cl, 1             // Once again shifting bits of ECX's low bit field to create "randomness", though this time the bits are being shuffled 
      ror   cl, 1             // right. Twice. The furthest right value loops to become the furthest left.
      mov   [eax], edx        // Moving the contents of the EDX register to the address of the EAX register.
      mov   eax, ecx          // Moving the more randomised encrypted character into the EAX register for future encryption.
      add   eax, 0x10         // (0x(000000)10 = 16 = 0001 0000) Adds the binary value of 16 to the EAX register. Adding "randomness" to the value.
      mov   edx, eax          // Moves the contents of EAX into EDX, which is being used as an output parameter. This is the output of this subroutine.

      mov   eax, edx          // Moving the complete encrypted key into EAX from EDX, as EAX is the output parameter.

      //------------------- End of Encryption Rountine Assembly Code ---------------------------

      add   esp, 8            // "Scrubs" the stacks of any parameters. Preventing these parameters from conflicting with other uses of the stack.
      mov   temp_char, al     // Moves the low 8-bit field of EAX into temp_char, saving the encrypted character. Not doing so would lose the character to the code below.

      pop   edx               // Popping the contents in the stack from EDX, ECX and EAX (the caller registers) back into their respective registers 
      pop   ecx               // to recover the values saved before the encryption routine began, preventing unintended errors from occurring due to left-over data.
      pop   eax


      lea   ebx, [encrypted_chars + ecx]    // Get the position of the current char from encrypted_chars array and put it's address in ebx.
      movzx eax, temp_char    // Move the character from temp char into eax and pad it with zeros to fit the correct size of eax.
      mov   byte ptr[ebx], al // Move the bits containing the character from eax into the address of the current char in the encrypted_chars array.

      jmp   forloop           // Jumping back to the start of the forloop. (This is the end of said forloop.)

      endfor:
      pop   eax               // Popping back the registers used in the forloop that were pushed before it's execution for safety.
      pop   ebx
      pop   ecx
    }

  return;
}
//---------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------
//----------------- DECRYPTION ROUTINE --------------------------------------------------------------------------

void decrypt_chars(int length, char EKey, char* encrypted_chars, char* decrypted_chars)
{
  char temp_char;             // Character Temporary Store

    for (int i = 0; i < length; ++i)    // Decrypt Characters one at a time
    {
        temp_char = encrypted_chars[i]; // Get the next char from encrypted_chars array

        __asm
        {
          push   eax          // Pushing EAX's contents onto the stack to be saved as it'll be used as an input parameter for the encryption key.
          push   ecx          // ECX is also pushed as it'll used as an input parameter for the to-be decrypted character. EDX is pushed as it'll
          push   edx          // be used as the output parameter for the decryption routine. (All of the caller registers are pushed.)

          movzx  ecx, temp_char// Moving the to-be decrypted character into the ECX, padding out the empty bits with 0s to allow it to fit within the register correctly.
          lea    eax, EKey    // Puts the address of the Encryption Key into the EAX register to be pushed to the stack for later use.
          push   eax          // Pushes the EAX onto the stack so it can be used as part of the base pointer code in the decryption routine later.
          push   ecx          // The same happens for ECX.
          call   decrypt_10   // Calls the decryption routine which runs the instructions that decrypts the code.

          add    esp, 8       // "Scrubs" the stacks of any parameters. Preventing these parameters from conflicting with other uses of the stack.
          mov    temp_char, al// Moves the low 8-bit field of EAX into temp_char, saving the decrypted character. Not doing so would lose the character to the code below.

          pop    edx          // Popping the contents in the stack from EDX, ECX and EAX (the caller registers) back into their respective registers 
          pop    ecx          // to recover the values saved before the decryption routine began, preventing unintended errors from occurring due to left-over data.
          pop    eax
        }

        decrypted_chars[i] = temp_char; // Stores the Decrypted Character in the decrypted_chars array.
    }

    return;

    //------------------------- Decryption Rountine - No.10 -----------------------------------
    // This code decrypts a given character (ECX) using the given encryption key from the EAX
    // register. This is intended to be repeated in a for loop to complete an entire string.
    //
    // Inputs: register   EAX = The 32-bit address of the Ekey.
    //                    ECX = The character to be decrypted (in the low 8-bit field, CL).
    // Output: register   EAX = The decrypted value of the source character (in the low 8-bit field, AL).
    __asm
    {
    decrypt_10:

      push  ebp               // Pushing the base pointer onto the stack to save it's contents, as the code below will overwrite it.
      mov   ebp, esp          // Moving the stack pointer into the base pointer register to use as a reference point for the parameters.
      mov   ecx, [ebp + 8]    // Retrieving one of the two parameters from the stack back into ECX, this one is the to-be decrypted character.
      mov   eax, [ebp + 12]   // Retrieving the other parameter back into EAX, this one is the Encryption Key.

      push  ecx               // Pushing the to-be decrypted character to the stack to allow the use of the ECX register for finding the ECX is later subtracted by.

      mov   ecx, [eax]        // These 5 lines of code are near identical to the earlier lines of code in the encryption routine. This is because it uses the encryption
      and   ecx, 0x000000A7   // key to find the aforementioned value that ECX is subtracted by later in this routine. (Henceforth called the "subtraction value".)
      rol   cl, 1             // "mov ecx, [eax]" pulls information from the EKey, which may or may not have been changed by prior decryptions to 'reverse' the "randomness"
                              // of the encryption.
      add   ecx, 0x03
      mov   edx, ecx          // Moving the "subtraction value" into the EDX register as to not lose it when the to-be-decrypted is popped back off of the stack back into the ECX register.

      pop   ecx               // Reclaiming the prior contents of the ECX register from the stack now that the "subtraction value" that was used in the encryption has been found.

      sub   ecx, 0x10         // The following five instructions are reversing the later parts of the encryption routine, with this one reversing "add eax, 0x10".
      rol   cl, 1             // Reversing the two "ror cl, 1" instructions from the encryption routine.
      rol   cl, 1
      sub   ecx, edx          // Finally subtracting the "subtraction value" from the to-be-decrypted character in the ECX register.
      mov   [eax], edx        // Making changes to the EKey, in a similar fashion to the encryption routine, for the subsequent decryptions by moving the contents of EDX into the
                              // address of EAX.

      mov   eax, ecx          // Moving the decrypted character into the EAX register, which is used as the output parameter.
      mov   esp, ebp          // Moving the base pointer back into the stack pointer register, reversing/reseting what was done earlier.
      pop   ebp               // Popping the prior contents of EBP off of the stack back into it's register to return the register to it's original state.

      ret                     // Jumps back to line after the calling of this encryption rountine to end said routine and make use of the output (EAX).
    }
}
//---------------------------------------------------------------------------------------------------------------