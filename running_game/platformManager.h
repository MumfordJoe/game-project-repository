#pragma once

// Class Headers
#include "platform.h"

// Misc Headers
#include "utilities.h"

//------------------Platform Manager Class Declaration Start//
class PlatformManager
{
public:
	PlatformManager(MyD3D& d3d);																// Constructor
	~PlatformManager();																			// Destructor

	void init();
	void spawnPlatform();																		// Creates a platform and adds it to the platform_instance vector array
	void spawnPlatform(float xPos, float yPos, float minScale, float maxScale, bool randScale);	// Creates a platform at a position with a scale and adds it to the platform_instance vector array
	void spawnTimer(float elapsed, float environmentSpeed);																// Creates platforms at calculated intervals
	void checkDeletePlatform(Platform* platform, int i);										// Checks a platform's current position and deletes it when necessary
	void clearPlatforms();																		// Deletes all platforms and empties platform_instances
	void destroy();																				// Deletes this instance of PlatformManager

	std::vector < Platform* > platform_instances{};												// Holds and points to all of the platforms that have been created through spawnPlatform()
	float platformSpawnTimer = 0;																// The time until the next platform is spawned
	
private:
	MyD3D& mD3D;

	float maxPlatformSpawnTimerLength = GC::PLATFORM_SPAWNTIMER_MAX;							// The maximum values for how long it takes until the next platform is spawned
	float minPlatformSpawnTimerLength = GC::PLATFORM_SPAWNTIMER_MIN;							// The minimum values for how long it takes until the next platform is spawned
	float maxPlatformXPosition = GC::PLATFORM_XPOS_MAX;											// The maximum x position the platform manager can spawn a platform at
	float minPlatformXPosition = GC::PLATFORM_XPOS_MIN;											// The minimum x position the platform manager can spawn a platform at
	float maxPlatformYPosition = GC::PLATFORM_YPOS_MAX;											// The maximum y position the platform manager can spawn a platform at
	float minPlatformYPosition = GC::PLATFORM_YPOS_MIN;											// The minimum y position the platform manager can spawn a platform at
	float maxPlatformWidth = GC::PLATFORM_WIDTH_MAX;											// The maximum width a platform can be
	float minPlatformWidth = GC::PLATFORM_WIDTH_MIN;											// The minimum width a platform can be
	float lastSpawnedPlatformsHeight = GC::PLATFORM_YPOS_MIN;									// Stores the height of the last spawned platform, which is used to prevent unscalable walls from popping up
};
//------------------Platform Manager Class Declaration End//