#pragma once

// Include Libraries

// DX11 Libraries
#pragma once

// DX11 Headers
#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "DDSTextureLoader.h"

#include <chrono>
#include <iostream>
#include <ratio>
#include <thread>

#include "varTypes.h"
#include "iconResource.h"

//------------------Debug Functions Declaration Start//
void PrintToOutput(const std::string& msg1, const std::string& msg2 = "");							// Prints a string to the build/output window upon building
float clamp(float num, float lower, float upper);													// Ensures the given number is between the given bounds
void encryptDecrypt(std::string& toEncrypt, char key);
void moduloEncrypt(std::string& toEncrypt, char key);
void moduloDecrypt(std::string& toEncrypt, char key);
void moduloEncryptInt(int& toEncrypt, char key);
void moduloDecryptInt(int& toEncrypt, char key);
//------------------Debug Functions  Declaration End//

//------------------Collider Class Declaration Start//
class Collider
{
public:
	bool isColliding(Sprite object1, Sprite object2, bool firstIsRect, bool secondIsCircle);		// Returns true if the two given sprite objects are colliding
};
//------------------Collider Class Declaration Start//

//------------------File Loader Class Declaration Start//
class FileLoader
{
public:
    ID3D11ShaderResourceView* loadTexture(const std::string& texLocation, MyD3D& d3d);				// Loads a texture from a file
    SpriteFont* loadFont(const std::wstring& fileLocation, MyD3D& d3d);						    	// Loads a font from a file
	void assignTexToSpr(const std::string& texLocation, MyD3D& d3d, Sprite& spr);	                // Assigns a texture from a file to a sprite
	void assignFontToText(std::wstring fontLocation, MyD3D& d3d, Text& text);	                    // Assigns a font from a file to a string of text
	HICON loadIcon(HINSTANCE hInstance);
};
//------------------File Loader Class Declaration End//

//------------------FPS Declaration Start//
class FramerateManager
{
public:
	FramerateManager(const int FPS)
		: fpsLimit(FPS)
	{
		time_t now = time(0);
		thisFrame = std::chrono::system_clock::now();
		lastFrame = std::chrono::system_clock::now();
	}
	bool update()
	{
		lastFrame = thisFrame;
		return sleep(thisFrame - lastFrame);
	}
	void initGameTime()
	{
		time_t now = time(0);
		thisFrame = std::chrono::system_clock::now();
		lastFrame = std::chrono::system_clock::now();
	}
	void setFPS(int FPS)
	{
		fpsLimit = FPS;
	}
private:
	bool sleep(std::chrono::duration<double, std::milli> deltaTime)
	{
		if (deltaTime.count() < fpsLimit)
		{
			std::chrono::duration<double, std::milli> delta_ms(fpsLimit - deltaTime.count());
			auto delta_ms_duration = std::chrono::duration_cast<std::chrono::milliseconds>(delta_ms);
			std::this_thread::sleep_for(std::chrono::milliseconds(delta_ms_duration.count()));
			return false;
		}
		return true;
	}

	std::chrono::system_clock::time_point thisFrame;
	std::chrono::system_clock::time_point lastFrame;
	int fpsLimit;
	int frameDelay = 1 / fpsLimit;		//std::chrono::milliseconds frameDelay = std::chrono::milliseconds((1 / fpsLimit) * 1000);
};
//------------------FPS Declaration End//