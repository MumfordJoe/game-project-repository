// Accompanying Header
#include "outro.h"

// DX11 Headers
#include "Input.h"

// Class Headers
#include "timer.h"

// Misc. Headers
#include "utilities.h"
#include "gameConst.h"

//------------------Scoreboard Class Functions Start//
//////// Public: ////////

// Constructor
Scoreboard::Scoreboard(MyD3D& d3d)
	:mD3D(d3d), scoreboardTitle(mD3D), scoreboardText(mD3D), drawnName(mD3D), timer(mD3D)
{
	initScoreboard();
}

void Scoreboard::initScoreboard()
{
	FileLoader fileLoader;

	// Load Scoreboard Title
	fileLoader.assignFontToText(FILELOC::FONT_CHINYEN_58, mD3D, scoreboardTitle);
	scoreboardTitle.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	scoreboardTitle.setString("High Scores");
	RECT texR0 = scoreboardTitle.getGlobalBounds();
	scoreboardTitle.setOrigin((texR0.right - texR0.left) / 2.f, (texR0.top - texR0.bottom / 2.f));
	//scoreboardTitle.setStyle(sf::Text::Bold);
	scoreboardTitle.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.125f);

	// Load Scoreboard Contents
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_36, mD3D, scoreboardText);
	scoreboardText.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	scoreboardText.setString("___");
	RECT texR1 = scoreboardText.getGlobalBounds();
	scoreboardText.setOrigin((texR1.right - texR1.left) / 2.f, (texR1.top - texR1.bottom / 2.f));
	//scoreboardText.setStyle(sf::Text::Bold);
	scoreboardText.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.425f);

	// Display player's name being typed out
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, drawnName);
	drawnName.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	drawnName.setString("<Please insert your name.>");
	RECT texR2 = drawnName.getGlobalBounds();
	drawnName.setOrigin((texR2.right - texR2.left) / 2.f, (texR2.top - texR2.bottom / 2.f));
	//scoreboardText.setStyle(sf::Text::Bold);
	drawnName.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.425f);

	keyDown = true;
	isScoreUpdated = false;
	newScore = 0;
	Timer timer(mD3D);
}

void Scoreboard::initScoreboard(int newScoreIn)
{
	FileLoader fileLoader;

	// Load Scoreboard Title
	fileLoader.assignFontToText(FILELOC::FONT_CHINYEN_58, mD3D, scoreboardTitle);
	scoreboardTitle.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	scoreboardTitle.setString("High Scores");
	RECT texR0 = scoreboardTitle.getGlobalBounds();
	scoreboardTitle.setOrigin((texR0.right - texR0.left) / 2.f, (texR0.top - texR0.bottom / 2.f));
	//scoreboardTitle.setStyle(sf::Text::Bold);
	scoreboardTitle.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.125f);

	// Load Scoreboard Contents
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_36, mD3D, scoreboardText);
	scoreboardText.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	scoreboardText.setString("___");
	RECT texR1 = scoreboardText.getGlobalBounds();
	scoreboardText.setOrigin((texR1.right - texR1.left) / 2.f, (texR1.top - texR1.bottom / 2.f));
	//scoreboardText.setStyle(sf::Text::Bold);
	scoreboardText.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.425f);

	// Display player's name being typed out
	fileLoader.assignFontToText(FILELOC::FONT_DIGITAL7_48, mD3D, drawnName);
	drawnName.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1)); // White
	drawnName.setString("<Please insert your name.>");
	RECT texR2 = drawnName.getGlobalBounds();
	drawnName.setOrigin((texR2.right - texR2.left) / 2.f, (texR2.top - texR2.bottom / 2.f));
	//scoreboardText.setStyle(sf::Text::Bold);
	drawnName.setPosition(GC::SCREEN_RES.x * 0.5, GC::SCREEN_RES.y * 0.425f);

	keyDown = true;
	isScoreUpdated = false;
	newScore = newScoreIn;
	Timer timer(mD3D);
}

bool Scoreboard::renderScoreboard(DirectX::SpriteBatch& batch, EGameMode& _eGameMode, MouseAndKeys& mouseAndKeys, SFXManager& sfxManager, float dTime)
{
	// Clear Window Screen
	mD3D.BeginRender(GC::MENU_COLOR);

	CommonStates dxstate(&mD3D.GetDevice());
	batch.Begin(DirectX::SpriteSortMode_Deferred, dxstate.NonPremultiplied());

	// Scoreboard Navigation & Database Update
	if(!isNameInput || (newName.empty()))
	{
		isNameInput = scoreboardInput(newName, mouseAndKeys, batch, sfxManager);
		drawnName.draw(batch);
	}
	else
	{
		if (!(continueTimer <= 0))
			continueTimer -= dTime;

		if (continueTimer <= 0)
		{
			if ((mouseAndKeys.IsPressed(VK_RETURN)) || (mouseAndKeys.IsPressed(VK_SPACE)))
			{
				if (keyDown == false)
				{
					_eGameMode = EGameMode::Intro;
					batch.End();
					mD3D.EndRender();
					sfxManager.playSFX(4);
					return false;
				}
				keyDown = true;
			}
			else
			{
				keyDown = false;
			}
		}

		if (!isScoreUpdated)
		{
			scoreboardUpdate();
			isScoreUpdated = true;
		}

		// Fetching and Drawing Scoreboard Content
		float y = GC::SCREEN_RES.y * 0.3;
		for (size_t i = 0; i < 10; ++i) {
			stringstream ss;
			string name;
			int score = 0;
			int sessions = 0;
			if (metrics.playerData.size() > i) {
				name = metrics.playerData[i].name.substr(0, 10);
				score = metrics.playerData[i].score;
				sessions = metrics.playerData[i].sessions;
			}
			ss << setw(4) << setfill(' ') << i + 1;
			float x = GC::SCREEN_RES.x * 0.05f;
			scoreboardText.setPosition(x, y);
			scoreboardText.setString(ss.str());
			scoreboardText.draw(batch);

			x = GC::SCREEN_RES.x * 0.17f;
			scoreboardText.setPosition(x, y);
			scoreboardText.setString(name);
			scoreboardText.draw(batch);

			x = GC::SCREEN_RES.x * 0.42f;
			scoreboardText.setPosition(x, y);
			ss.str("");
			ss << score << " Points";
			scoreboardText.setString(ss.str());
			scoreboardText.draw(batch);

			x = GC::SCREEN_RES.x * 0.67f;
			scoreboardText.setPosition(x, y);
			ss.str("");
			ss << sessions << " Sessions Played";
			scoreboardText.setString(ss.str());
			scoreboardText.draw(batch);

			y += (scoreboardText.getGlobalBounds().bottom - scoreboardText.getGlobalBounds().top) * 1.2f;
		}
	}

	// Drawing Menu Text
	scoreboardTitle.draw(batch);

	// Updating the Window
	batch.End();
	mD3D.EndRender();

	return true;
}

void Scoreboard::destroy()
{
	delete this;
}

std::string wstringToString(const std::wstring& s);

bool Scoreboard::scoreboardInput(std::string& newName, MouseAndKeys& mouseAndKeys, DirectX::SpriteBatch& batch, SFXManager& sfxManager)
{
	if (newName.size() > 1 && mouseAndKeys.IsPressed(VK_RETURN))
	{
		if (keyDown == false)
		{
			sfxManager.playSFX(1);
			return true;
		}
		keyDown = true;
	}
	else
	{
		keyDown = false;
	}

	std::string input;
	typing.getKeyboard(input, mouseAndKeys);
	newName += input;


	timer.update();
	int seconds = timer.getSeconds();
	if (newName.size() > 0)
	{
		if (seconds % 2 == 1)
		{
			drawnName.setString(newName);
		}
		if (seconds % 2 == 0)
		{
			drawnName.setString(newName + "|");
			drawnName.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 0, 1));
			drawnName.draw(batch);
			drawnName.setString(newName);
			drawnName.setFillColor(DirectX::SimpleMath::Vector4(1, 1, 1, 1));
		}
	}

	return false;
}

void Scoreboard::scoreboardUpdate()
{
	metrics.Load(FILELOC::SCOREBOARD, true);

	metrics.Restart();

	metrics.name = newName;
	metrics.score = newScore;

	metrics.SortAndUpdatePlayerData();
	metrics.Save();

	metrics.DBClose();
}

//////// Private: ////////

//------------------Scoreboard Class Functions End//

// Wstring Conversion
std::string wstringToString(const std::wstring& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
	char* buf = new char[len];
	WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, buf, len, 0, 0);
	std::string r(buf);
	delete[] buf;
	return r;
}