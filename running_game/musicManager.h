#pragma once

// Include Libraries
#include "AudioMgrFMOD.h"
#include "FileUtils.h"

// Class Libraries
#include "eGameMode.h"

// Misc. Headers
#include "gameConst.h"

//------------------Music Manager Class Declaration Start//
class MusicManager
{
public:
	MusicManager()							// Constructor
	{
		File::initialiseSystem();
		audio.Initialise();
	};

	void playMusic(EGameMode _eGameMode)
	{
		if (!audio.GetSongMgr()->IsPlaying(musicHdl))
		{
			switch (_eGameMode)
			{
			case (EGameMode::Intro):
			{
				audio.GetSongMgr()->Play(FILELOC::MUSIC_MENU, true, false, &musicHdl, 0.1f);
				break;
			}
			case (EGameMode::Game):
			{
				audio.GetSongMgr()->Play(FILELOC::MUSIC_GAME, true, false, &musicHdl, 0.15f);
				break;
			}
			case (EGameMode::Outro):
			{
				audio.GetSongMgr()->Play(FILELOC::MUSIC_SCOREBOARD, true, false, &musicHdl, 0.15f);
				break;
			}
			}
		}
	}

	void stopMusic()
	{
		if (audio.GetSongMgr()->IsPlaying(musicHdl))
		{
			audio.GetSongMgr()->Stop();
		}
	}

	void update()
	{
		audio.Update();
	}

	//MyD3D& mD3D;

private:
	AudioMgrFMOD audio;
	unsigned int musicHdl;
};
//------------------ Music Manager Class Declaration Start//