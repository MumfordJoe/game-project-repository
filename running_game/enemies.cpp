#pragma once

// Accompanying Header
#include "enemies.h"

// Class Headers

// Misc. Headers
#include "randomNumberGenerator.h"

//------------------Enemy Class Functions Start//
//////// Public: ////////

// Default Constructor
Enemy::Enemy(MyD3D& d3d)
	:mD3D(d3d), spr(mD3D), currentHealth(0)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_ENEMY, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin((texR.right - texR.left) / 2, (texR.bottom - texR.top / 2));
	spr.setScale(1.5, 1.5);
	spr.setPosition(GC::SCREEN_RES.x * 1.15f, GC::SCREEN_RES.y * 0.335f);	// Sets the position of the object when created.

	currentHealth = maxHealth;
}

Enemy::Enemy(float xPos, float yPos, MyD3D& d3d)
	:mD3D(d3d), spr(mD3D), currentHealth(0)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_ENEMY, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin((texR.right - texR.left) / 2, (texR.bottom - texR.top / 2));
	spr.setScale(1.5, 1.5);
	spr.setPosition(xPos, yPos);	// Sets the position of the object when created.

	currentHealth = maxHealth;
}

// Movement Code
void Enemy::move(float elapsed)
{
	DirectX::SimpleMath::Vector2 pos = spr.getPosition();

	if ((pos.x > GC::SCREEN_RES.x / 2) && (isHovering == false))
	{
		pos.x -= speed * elapsed;
		hoveringCenter = pos.x;
	}
	else
	{
		if (isHovering == false)
		{
			isHovering = true;
		}

		hoverValue += elapsed;
		if (hoverSpeed_Current < hoverSpeed_Max)
		{
			hoverSpeed_Current += hoverSpeed_Max * hoverAcceleration;
			if (hoverSpeed_Current > hoverSpeed_Max)
			{
				hoverSpeed_Current = hoverSpeed_Max;
			}
		}

		pos.x = (hoveringCenter + (sin(hoverValue * hoverSpeed_Current * (speed / GC::ENEMY_SPEED)) * GC::ENEMY_HOVERINGWIDTH));
	}

	spr.setPosition(pos);

	if (pos.x < 10)
	{
		deleteMe == true;
	}
}

void Enemy::attackTimer(float elapsed, float environmentSpeed, ObstacleManager& obstacleManager, SFXManager& sfxManager)
{
	if (crateAttackTimer <= 0)
	{
		obstacleManager.spawnCrate(spr.getPosition().x, spr.getPosition().y, crateScale, crateScale, false);
		
		sfxManager.playSFX(6);

		crateAttackTimer = RandomNumberGenerator::getRandomFloat(attackTimer_Min * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed), attackTimer_Max * (GC::ENVIRONMENTOBJECT_SPEED / environmentSpeed));
	}
	else
	{
		crateAttackTimer -= elapsed;;

		if (crateAttackTimer < 0)
			crateAttackTimer = 0;
	}
}

//// Collision Code
//void Enemy::collide(float elapsed, Sprite collidingSpr, bool isPlatform)
//{
//	DirectX::SimpleMath::Vector2 pos = spr.getPosition();
//	DirectX::SimpleMath::Vector2 collidingPos = collidingSpr.getPosition();
//
//	if (!isPlatform)
//	{
//		isColliding = enemyCollider.isColliding(spr, collidingSpr);
//
//		if (isColliding)
//		{
//			//gravity_current = 0;
//			collidingPos.x -= speed * 1.25 * elapsed;
//		}
//
//		collidingSpr.setPosition(collidingPos);
//	}
//	else
//	{
//		gravity_current = 0;
//
//		pos.y -= GC::CRATE_GRAVITY * 3.25 * elapsed;
//
//		if ((pos.y/* - spr.getOrigin().y*/) > (collidingPos.y))
//		{
//			pos.x -= (speed) * 1.25 * elapsed;
//		}
//		spr.setPosition(pos);
//	}
//}

void Enemy::deleteInstance()
{
	delete this;
}

void Enemy::updateSpeed(float speedIn)
{
	speed = speedIn * 0.85;
}

//------------------Enemy Class Functions End//

//------------------Boss Class Functions Start//
//////// Public: ////////
// Default Constructor
Boss::Boss(MyD3D& d3d)
	:Enemy(d3d)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_BOSS, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin((texR.right - texR.left) / 2, (texR.bottom - texR.top / 2));
	spr.setScale(2.15, 2.15);
	spr.setPosition(GC::SCREEN_RES.x * 1.15f, GC::SCREEN_RES.y * 0.335f);	// Sets the position of the object when created.

	currentHealth = maxHealth;
}

Boss::Boss(float xPos, float yPos, MyD3D& d3d)
	:Enemy(d3d)
{
	FileLoader fileLoader;

	fileLoader.assignTexToSpr(FILELOC::SPRITE_BOSS, mD3D, spr);
	RECTF texR = spr.getTextureRect();
	spr.setOrigin((texR.right - texR.left) / 2, (texR.bottom - texR.top / 2));
	spr.setScale(2.15, 2.15);
	spr.setPosition(xPos, yPos);	// Sets the position of the object when created.

	currentHealth = maxHealth;
}
//------------------Boss Class Functions End//